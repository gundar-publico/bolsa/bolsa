package com.gundar.bolsa;

import java.util.List;

/**
 * Lyxor IBEX 35 Inverso Diario UCITS ETF - Acc
 */
public class CargadorFR0010762492Csv {

  private static final String NOMBRE_ARCHIVO = "FR0010762492.csv";

  private final CargadorCsv cargadorCsv;

  public CargadorFR0010762492Csv() {
    this.cargadorCsv = new CargadorCsv(true);
  }

  public List<InformacionDia> cargar() {
    return cargadorCsv.cargar(NOMBRE_ARCHIVO);
  }

}
