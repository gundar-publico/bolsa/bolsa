package com.gundar.bolsa.calculos;


public class Ema extends MediaMovil {

  public Ema(double mediaInicial, int periodo) {
    super(mediaInicial, new FormulaEma(periodo));
  }

  public Ema(int periodo) {
    super(new FormulaEma(periodo));
  }

}
