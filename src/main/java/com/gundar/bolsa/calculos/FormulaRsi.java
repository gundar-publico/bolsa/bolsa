package com.gundar.bolsa.calculos;


public class FormulaRsi {

  public double calcular(double mediaGanancias, double mediaPerdidas) {

    /* Por definición de la fórmula */
    if (Double.compare(mediaPerdidas, 0) == 0) {
      return 100;
    }

    /* Por definición de la fórmula */
    if (Double.compare(mediaGanancias, 0) == 0) {
      return 0;
    }

    double valorRs = formulaRs(mediaGanancias, mediaPerdidas);

    return formulaRsi(valorRs);
  }

  private double formulaRsi(double valorRs) {

    return 100 - (100.0 / (1 + valorRs));
  }

  private double formulaRs(double mediaGanancias, double mediaPerdidas) {

    return mediaGanancias / mediaPerdidas;
  }

  @Override
  public String toString() {
    return "FormulaRsi []";
  }

}
