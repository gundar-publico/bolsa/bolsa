package com.gundar.bolsa.calculos;


public class FormulaSma implements FormulaMedia {

  private final int periodo;

  public FormulaSma(int periodo) {
    this.periodo = periodo;
  }

  @Override
  public double calcular(double mediaAnterior, double valorActual) {

    return mediaAnterior + ((valorActual - mediaAnterior) / periodo);
  }

  @Override
  public String toString() {
    return "FormulaSma [periodo=" + periodo + "]";
  }

}
