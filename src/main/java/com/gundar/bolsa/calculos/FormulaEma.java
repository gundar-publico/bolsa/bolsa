package com.gundar.bolsa.calculos;


public class FormulaEma implements FormulaMedia {

  private final double k;

  public FormulaEma(int periodo) {

    k = 2.0 / (periodo + 1);
  }

  @Override
  public double calcular(double mediaAnterior, double valorActual) {

    return mediaAnterior + (k * (valorActual - mediaAnterior));
  }

  @Override
  public String toString() {
    return "FormulaEma [k=" + k + "]";
  }

}
