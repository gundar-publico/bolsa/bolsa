package com.gundar.bolsa.calculos;

import java.util.Collection;
import java.util.Queue;

import org.apache.commons.collections4.queue.CircularFifoQueue;

import com.gundar.bolsa.InformacionDia;
import com.gundar.bolsa.simulacion.configuracion.ConfiguracionRsiAvanzado;

public class RsiAvanzado {

  private Rsi rsi;

  private final Queue<Double> elementos;

  private double media;
  private double varianza;

  private final ConfiguracionRsiAvanzado configuracionRsiAvanzado;

  public RsiAvanzado(Collection<InformacionDia> informacionDias, ConfiguracionRsiAvanzado configuracionRsiAvanzado) {

    this.rsi = new Rsi(informacionDias, configuracionRsiAvanzado.getConfiguracionRsi());
    this.elementos = new CircularFifoQueue<Double>(configuracionRsiAvanzado.getPeriodo());
    this.media = rsi.getValor();
    this.varianza = 0;
    this.configuracionRsiAvanzado = configuracionRsiAvanzado;
  }

  public RsiAvanzado(Queue<Double> valoresRsiAnteriores, ConfiguracionRsiAvanzado configuracionRsiAvanzado) {

    this.rsi = new Rsi(configuracionRsiAvanzado.getConfiguracionRsi());
    this.configuracionRsiAvanzado = configuracionRsiAvanzado;

    this.elementos = new CircularFifoQueue<>(configuracionRsiAvanzado.getPeriodo());
    this.elementos.addAll(valoresRsiAnteriores);
  }

  public double calcular(RsiAvanzado rsiAvanzadoAnterior, double variacionPrecio) {

    double nuevoRsi = rsi.calcular(rsiAvanzadoAnterior.rsi, variacionPrecio);

    addNuevoValor(nuevoRsi);

    return nuevoRsi;
  }

  private void addNuevoValor(double valorRsi) {

    if (elementos.size() < configuracionRsiAvanzado.getPeriodo()) {

      addNuevoValorColaNoLlena(valorRsi);
    } else {

      assert elementos.size() == configuracionRsiAvanzado.getPeriodo();

      addNuevoValorColaLlena(valorRsi);
    }

  }

  private void addNuevoValorColaNoLlena(double valorRsi) {

    final double delta = valorRsi - media;
    media = media + (delta / elementos.size());
    varianza += delta * (valorRsi - media);
  }

  private void addNuevoValorColaLlena(double valorRsiNuevo) {

    int n = elementos.size();

    double valorRsiSaliente = elementos.poll();

    double mediaAnterior = media;
    media = media + ((valorRsiNuevo - valorRsiSaliente) / n);

    varianza = varianza + (((((valorRsiNuevo - media) + valorRsiSaliente) - mediaAnterior) * (valorRsiNuevo - valorRsiSaliente)) / (n - 1));
  }

  public double getValor() {
    return rsi.getValor();
  }

  public double getBandaSuperior() {
    return getValor() + (calculaDesviacionTipica() * configuracionRsiAvanzado.getMultiplicadorBandas());
  }

  public double getBandaInferior() {
    return getValor() - (calculaDesviacionTipica() * configuracionRsiAvanzado.getMultiplicadorBandas());
  }

  private double calculaDesviacionTipica() {
    return Math.sqrt(varianza);
  }

}
