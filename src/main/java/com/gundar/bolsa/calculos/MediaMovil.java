package com.gundar.bolsa.calculos;


public class MediaMovil {

  // public static final int NUM_VALORES_MEDIA_INICIAL = 5;

  private Double media;

  private final FormulaMedia formulaMedia;

  public MediaMovil(FormulaMedia formulaMedia) {
    super();
    this.media = null;
    this.formulaMedia = formulaMedia;
  }

  public MediaMovil(double mediaInicial, FormulaMedia formulaMedia) {
    super();
    this.media = mediaInicial;
    this.formulaMedia = formulaMedia;
  }

  public double calcular(MediaMovil mediaAnterior, double valorActual) {

    if (media != null) {
      return media;
    }

    media = formulaMedia.calcular(mediaAnterior.media, valorActual);

    return media;
  }

  @Override
  public String toString() {
    return "MediaMovil [media=" + media + ", formulaMedia=" + formulaMedia + "]";
  }

  public double getMedia() {
    return media;
  }

}
