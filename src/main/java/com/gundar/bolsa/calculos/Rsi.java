package com.gundar.bolsa.calculos;

import java.util.Collection;
import java.util.Iterator;

import com.gundar.bolsa.InformacionDia;
import com.gundar.bolsa.simulacion.configuracion.ConfiguracionRsi;

public class Rsi {

  private MediaMovil mediaGanancias;
  private MediaMovil mediaPerdidas;
  private final FormulaRsi formulaRsi;

  private Double valor;

  public Rsi(ConfiguracionRsi configuracionRsi) {
    super();
    mediaGanancias = new Sma(configuracionRsi.getPeriodo());
    mediaPerdidas = new Sma(configuracionRsi.getPeriodo());
    formulaRsi = new FormulaRsi();
  }

  public Rsi(double mediaInicialGanancias, double mediaInicialPerdidas, ConfiguracionRsi configuracionRsi) {
    super();
    mediaGanancias = new Sma(mediaInicialGanancias, configuracionRsi.getPeriodo());
    mediaPerdidas = new Sma(mediaInicialPerdidas, configuracionRsi.getPeriodo());
    formulaRsi = new FormulaRsi();
    valor = formulaRsi.calcular(mediaGanancias.getMedia(), mediaPerdidas.getMedia());
  }

  public Rsi(Collection<InformacionDia> informacionDias, ConfiguracionRsi configuracionRsi) {

    final int periodo = configuracionRsi.getPeriodo();

    double sumaGanancias = 0;
    double sumaPerdidas = 0;

    int indice = 1;

    Iterator<InformacionDia> iteradorInformacion = informacionDias.iterator();

    while ((indice <= periodo) && iteradorInformacion.hasNext()) {

      InformacionDia informacionDia = iteradorInformacion.next();

      double diferenciaLineal = informacionDia.getDiferenciaLineal();

      boolean sonGanancias = Double.compare(diferenciaLineal, 0) >= 0;

      if (sonGanancias) {
        sumaGanancias = sumaGanancias + diferenciaLineal;
      } else {
        sumaPerdidas = sumaPerdidas + (-diferenciaLineal);
      }

      indice++;
    }

    double mediaInicialGanancias = sumaGanancias / periodo;

    double mediaInicialPerdidas = sumaPerdidas / periodo;

    this.mediaGanancias = new Sma(mediaInicialGanancias, periodo);
    this.mediaPerdidas = new Sma(mediaInicialPerdidas, periodo);
    this.formulaRsi = new FormulaRsi();

    this.valor = formulaRsi.calcular(mediaGanancias.getMedia(), mediaPerdidas.getMedia());
  }

  public double calcular(Rsi rsiAnterior, double variacionPrecio) {

    if (valor != null) {
      return valor;
    }

    boolean sonGanancias = variacionPrecio >= 0;

    final double gananciaActual;
    final double perdidaActual;

    if (sonGanancias) {
      gananciaActual = variacionPrecio;
      perdidaActual = 0;
    } else {
      gananciaActual = 0;
      perdidaActual = -variacionPrecio;

    }

    double valorMediaGanancias = mediaGanancias.calcular(rsiAnterior.mediaGanancias, gananciaActual);

    double valorMediaPerdidas = mediaPerdidas.calcular(rsiAnterior.mediaPerdidas, perdidaActual);

    valor = formulaRsi.calcular(mediaGanancias.getMedia(), mediaPerdidas.getMedia());

    return valor;
  }

  @Override
  public String toString() {
    return "Rsi [mediaGanancias=" + mediaGanancias + ", mediaPerdidas=" + mediaPerdidas + ", formulaRsi=" + formulaRsi + ", valor=" + valor
        + "]";
  }

  public double getValor() {
    return valor;
  }

}
