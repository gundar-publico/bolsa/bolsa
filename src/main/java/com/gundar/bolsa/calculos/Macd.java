package com.gundar.bolsa.calculos;

import java.util.Collection;
import java.util.Iterator;

import com.gundar.bolsa.InformacionDia;
import com.gundar.bolsa.simulacion.configuracion.ConfiguracionMacd;

public class Macd {

  private Ema corto;
  private Ema largo;

  private Ema senal;

  public Macd(ConfiguracionMacd configuracionMacd) {

    corto = new Ema(configuracionMacd.getPeriodoCorto());
    largo = new Ema(configuracionMacd.getPeriodoLargo());

    senal = new Ema(configuracionMacd.getPeriodoSenal());
  }

  public Macd(Collection<InformacionDia> informacionDias, ConfiguracionMacd configuracionMacd) {

    final int periodoCorto = configuracionMacd.getPeriodoCorto();
    final int periodoLargo = configuracionMacd.getPeriodoLargo();
    final int periodoSenal = configuracionMacd.getPeriodoSenal();

    double sumaCorta = 0;
    double sumaLarga = 0;

    int indice = 1;

    Iterator<InformacionDia> iteradorInformacion = informacionDias.iterator();

    while ((indice <= periodoLargo) && iteradorInformacion.hasNext()) {

      InformacionDia informacionDia = iteradorInformacion.next();

      double precio = informacionDia.getPrecioCierre();

      if (indice <= periodoCorto) {
        sumaCorta = sumaCorta + precio;
      }

      sumaLarga = sumaLarga + precio;

      indice++;
    }

    double mediaInicialCorta = sumaCorta / periodoCorto;

    double mediaInicialLarga = sumaLarga / periodoLargo;

    this.corto = new Ema(mediaInicialCorta, periodoCorto);
    this.largo = new Ema(mediaInicialLarga, periodoLargo);

    double valorMacdInicial = mediaInicialCorta - mediaInicialLarga;
    this.senal = new Ema(valorMacdInicial, periodoSenal);
  }

  public void calcular(Macd macdAnterior, double precioActual) {

    double mediaCorta = corto.calcular(macdAnterior.corto, precioActual);

    double mediaLarga = largo.calcular(macdAnterior.largo, precioActual);

    double valorMacd = mediaCorta - mediaLarga;

    double mediaSenal = senal.calcular(macdAnterior.senal, valorMacd);
  }

  public double getValorMacd() {

    return corto.getMedia() - largo.getMedia();
  }

  public double getValorSenal() {

    return senal.getMedia();
  }

  public double getHistograma() {

    return getValorMacd() - getValorSenal();
  }

  @Override
  public String toString() {
    return "Macd [corto=" + corto + ", largo=" + largo + ", senal=" + senal + "]";
  }

}
