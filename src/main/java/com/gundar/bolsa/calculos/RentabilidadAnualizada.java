package com.gundar.bolsa.calculos;

import java.util.ArrayList;
import java.util.List;

public class RentabilidadAnualizada {

  private List<Double> rentabilidadesAnuales = new ArrayList<>();

  public void addRentabilidadAnual(double rentabilidad) {
    rentabilidadesAnuales.add(rentabilidad);
  }

  public double calculo() {

    double productoFactoresAnuales = 1;

    for (double rentabilidadAnual : rentabilidadesAnuales) {

      double factorAnual = 1 + rentabilidadAnual;

      productoFactoresAnuales *= factorAnual;
    }

    int numeroAnyos = rentabilidadesAnuales.size();

    double rentabilidadAnualizada = Math.pow(productoFactoresAnuales, (1.0 / numeroAnyos)) - 1;

    return rentabilidadAnualizada;
  }

  public static double calculo(double valorInicial, double valorFinal, double numeroAnyos) {

    return Math.pow((valorFinal / valorInicial), (1.0 / numeroAnyos)) - 1;
  }

  public static void main(String[] args) {

    // RentabilidadAnualizada r = new RentabilidadAnualizada();
    //
    // r.addRentabilidadAnual(-0.1413);
    // r.addRentabilidadAnual(0.4787);
    // r.addRentabilidadAnual(0.1839);
    // r.addRentabilidadAnual(0.1659);
    // r.addRentabilidadAnual(-0.2695);
    // r.addRentabilidadAnual(0.3270);
    // r.addRentabilidadAnual(0.1901);
    // r.addRentabilidadAnual(0.3047);
    // r.addRentabilidadAnual(0.2405);
    // r.addRentabilidadAnual(-0.0461);
    // r.addRentabilidadAnual(-0.4471);
    //
    // System.out.println(r.calculo());

    System.out.println(RentabilidadAnualizada.calculo(9352, 10043, 1));
  }

}
