package com.gundar.bolsa.calculos;


public interface FormulaMedia {

  public double calcular(double mediaAnterior, double valorActual);

}
