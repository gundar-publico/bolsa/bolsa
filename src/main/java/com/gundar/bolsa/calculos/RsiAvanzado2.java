package com.gundar.bolsa.calculos;

import java.util.Collection;

import com.gundar.bolsa.InformacionDia;
import com.gundar.bolsa.simulacion.configuracion.ConfiguracionRsiAvanzado;

public class RsiAvanzado2 {

  private final Rsi rsi;
  private final MediaMovil media;
  private final Varianza varianza;

  private final ConfiguracionRsiAvanzado configuracionRsiAvanzado;

  public RsiAvanzado2(Collection<InformacionDia> informacionDias, ConfiguracionRsiAvanzado configuracionRsiAvanzado) {

    this.rsi = new Rsi(informacionDias, configuracionRsiAvanzado.getConfiguracionRsi());
    this.media = new Sma(rsi.getValor(), configuracionRsiAvanzado.getPeriodo());
    this.varianza = new Varianza(0, configuracionRsiAvanzado.getPeriodo());
    this.configuracionRsiAvanzado = configuracionRsiAvanzado;
  }

  public double calcular(RsiAvanzado2 rsiAvanzadoAnterior, double variacionPrecio) {

    double nuevoRsi = rsi.calcular(rsiAvanzadoAnterior.rsi, variacionPrecio);

    media.calcular(rsiAvanzadoAnterior.media, nuevoRsi);

    varianza.calcular(rsiAvanzadoAnterior.varianza, media.getMedia(), rsiAvanzadoAnterior.media.getMedia(), nuevoRsi);

    return nuevoRsi;
  }

  @Override
  public String toString() {
    return "RsiAvanzado2 [rsi=" + rsi + ", media=" + media + ", varianza=" + varianza + "]";
  }

  public double getValor() {
    return rsi.getValor();
  }

  public double getBandaSuperior() {
    return getValor() + (calculaDesviacionTipica() * configuracionRsiAvanzado.getMultiplicadorBandas());
  }

  public double getBandaInferior() {
    return getValor() - (calculaDesviacionTipica() * configuracionRsiAvanzado.getMultiplicadorBandas());
  }

  private double calculaDesviacionTipica() {
    return Math.sqrt(varianza.getVarianza());
  }

}
