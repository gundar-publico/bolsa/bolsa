package com.gundar.bolsa.calculos;


public class Sma extends MediaMovil {

  public Sma(double mediaInicial, int periodo) {
    super(mediaInicial, new FormulaSma(periodo));
  }

  public Sma(int periodo) {
    super(new FormulaSma(periodo));
  }

}
