package com.gundar.bolsa.calculos;


public class Varianza {

  private final int periodo;
  private Double varianza;

  public Varianza(int periodo) {
    this(0, periodo);
  }

  public Varianza(double valorInicial, int periodo) {
    this.varianza = valorInicial;
    this.periodo = periodo;
  }

  public double calcular(Varianza varianzaAnterior, double media, double mediaAnterior, double nuevoValor) {

    if (varianza != null) {
      return varianza;
    }

    varianza = (((periodo - 2) * varianzaAnterior.varianza) + ((nuevoValor - media) * (nuevoValor - mediaAnterior))) / (periodo - 1);

    return varianza;
  }

  public double getVarianza() {
    return varianza;
  }

}
