package com.gundar.bolsa;

import java.time.LocalDate;

public class InformacionDia {

  private LocalDate fecha;
  private double precioCierre;
  private Double precioApertura;
  private double diferenciaPorcentual;
  private double diferenciaLineal;

  public InformacionDia(LocalDate fecha, double precio, Double precioApertura, double diferenciaPorcentual, double diferenciaLineal) {
    super();
    this.fecha = fecha;
    this.precioCierre = precio;
    this.precioApertura = precioApertura;
    this.diferenciaPorcentual = diferenciaPorcentual;
    this.diferenciaLineal = diferenciaLineal;
  }

  public double calculaDiferenciaPorcentualApertura() {

    final double diferenciaPorcentualApertura;

    if ((precioApertura == null) || (Double.compare(precioApertura, 0) == 0)) {
      /* Si no hay precioCierre de apertura, se coge la variación desde el precioCierre de cierre de ayer */
      diferenciaPorcentualApertura = diferenciaPorcentual;
    } else {

      double diferenciaApertua = precioCierre - precioApertura;
      diferenciaPorcentualApertura = diferenciaApertua / precioApertura;
    }

    return diferenciaPorcentualApertura;
  }

  @Override
  public String toString() {
    return "InformacionDia [fecha=" + fecha + ", precioCierre=" + precioCierre + ", precioApertura=" + precioApertura + ", diferenciaPorcentual="
        + diferenciaPorcentual + ", diferenciaLineal=" + diferenciaLineal + "]";
  }

  public LocalDate getFecha() {
    return fecha;
  }

  public double getPrecioCierre() {
    return precioCierre;
  }

  public Double getPrecioApertura() {
    return precioApertura;
  }

  public double getDiferenciaPorcentual() {
    return diferenciaPorcentual;
  }

  public double getDiferenciaLineal() {
    return diferenciaLineal;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    long temp;
    temp = Double.doubleToLongBits(diferenciaLineal);
    result = (prime * result) + (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(diferenciaPorcentual);
    result = (prime * result) + (int) (temp ^ (temp >>> 32));
    result = (prime * result) + ((fecha == null) ? 0 : fecha.hashCode());
    temp = Double.doubleToLongBits(precioCierre);
    result = (prime * result) + (int) (temp ^ (temp >>> 32));
    result = (prime * result) + ((precioApertura == null) ? 0 : precioApertura.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (!(obj instanceof InformacionDia)) {
      return false;
    }
    InformacionDia other = (InformacionDia) obj;
    if (Double.doubleToLongBits(diferenciaLineal) != Double.doubleToLongBits(other.diferenciaLineal)) {
      return false;
    }
    if (Double.doubleToLongBits(diferenciaPorcentual) != Double.doubleToLongBits(other.diferenciaPorcentual)) {
      return false;
    }
    if (fecha == null) {
      if (other.fecha != null) {
        return false;
      }
    } else if (!fecha.equals(other.fecha)) {
      return false;
    }
    if (Double.doubleToLongBits(precioCierre) != Double.doubleToLongBits(other.precioCierre)) {
      return false;
    }
    if (precioApertura == null) {
      if (other.precioApertura != null) {
        return false;
      }
    } else if (!precioApertura.equals(other.precioApertura)) {
      return false;
    }
    return true;
  }

}
