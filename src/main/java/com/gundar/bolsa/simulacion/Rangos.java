package com.gundar.bolsa.simulacion;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.gundar.bolsa.InformacionDia;

public class Rangos {

  public static Rango getRangoCompleto(Collection<InformacionDia> informacionDias) {

    return getRangoFiltrado(informacionDias, LocalDate.MIN, LocalDate.MAX);
  }

  public static Rango getRangoFiltrado(Collection<InformacionDia> informacionDias, LocalDate fechaInicial, LocalDate fechaFinal) {

    LocalDate fechaInicialRango = null;
    LocalDate fechaFinalRango = null;

    for (InformacionDia informacionDia : informacionDias) {

      LocalDate fechaActual = informacionDia.getFecha();

      if (fechaActual.isBefore(fechaInicial)) {
        /* Avanzamos hasta la fecha inicial especificada */
        continue;
      }

      if (fechaActual.isAfter(fechaFinal)) {
        /* Si hemos superado ya la fecha final salimos del bucle */
        break;
      }

      if (fechaInicialRango == null) {
        fechaInicialRango = fechaActual;
      }

      fechaFinalRango = fechaActual;
    }

    return new Rango(fechaInicialRango, fechaFinalRango);
  }

  public static List<Rango> getRangosAgrupadosPorAnyos(Collection<InformacionDia> informacionDias, final int rangoAnyos) {

    return getRangosAgrupadosPorAnyos(informacionDias, rangoAnyos, LocalDate.MIN, LocalDate.MAX);
  }

  public static List<Rango> getRangosAgrupadosPorAnyos(Collection<InformacionDia> informacionDias,
      final int rangoAnyos, LocalDate fechaInicial, LocalDate fechaFinal) {

    List<Rango> rangosPorAnyos = agruparFechasPorAnyos(informacionDias, fechaInicial, fechaFinal);

    List<Rango> rangosPorPeriodo = new ArrayList<>();

    int inicio = 0;
    int fin = inicio + rangoAnyos;

    while ((fin - 1) < rangosPorAnyos.size()) {

      LocalDate fechaInicialRango = null;
      LocalDate fechaFinalRango = null;

      for (int i = inicio; i < fin; i++) {

        Rango rangoAnual = rangosPorAnyos.get(i);

        if (fechaInicialRango == null) {
          fechaInicialRango = rangoAnual.getFechaInicial();
        }

        fechaFinalRango = rangoAnual.getFechaFinal();
      }

      rangosPorPeriodo.add(new Rango(fechaInicialRango, fechaFinalRango));

      inicio++;
      fin++;
    }

    return rangosPorPeriodo;
  }

  private static List<Rango> agruparFechasPorAnyos(Collection<InformacionDia> informacionDias, LocalDate fechaInicial,
      LocalDate fechaFinal) {

    List<Rango> rango = new ArrayList<>();

    LocalDate fechaInicialRango = null;
    LocalDate fechaFinalRango = null;

    for (InformacionDia informacionDia : informacionDias) {

      LocalDate fechaActual = informacionDia.getFecha();

      if (fechaActual.isBefore(fechaInicial)) {
        /* Avanzamos hasta la fecha inicial especificada */
        continue;
      }

      if (fechaActual.isAfter(fechaFinal)) {
        /* Si hemos superado ya la fecha final salimos del bucle */
        break;
      }

      if (fechaInicialRango == null) {
        fechaInicialRango = fechaActual;
      }

      if (fechaInicialRango.getYear() == fechaActual.getYear()) {

        fechaFinalRango = fechaActual;

      } else {

        rango.add(new Rango(fechaInicialRango, fechaFinalRango));

        fechaInicialRango = fechaActual;
        fechaFinalRango = fechaActual;
      }
    }

    rango.add(new Rango(fechaInicialRango, fechaFinalRango));

    return rango;
  }

}
