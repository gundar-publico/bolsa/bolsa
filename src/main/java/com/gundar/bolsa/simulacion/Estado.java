package com.gundar.bolsa.simulacion;

public enum Estado {

  ENTRANDO(true),
  DENTRO(true),
  SALIENDO(false),
  FUERA(false);

  private boolean enMercado;

  private Estado(boolean enMercado) {
    this.enMercado = enMercado;
  }

  public boolean enMercado() {
    return enMercado;
  }

  public Estado actualizar(Accion accion) {

    final Estado nuevoEstado;

    switch (this) {
      case DENTRO:
      case ENTRANDO:
        if ((accion == Accion.ENTRAR) || (accion == Accion.MANTENER)) {
          nuevoEstado = DENTRO;
        } else {
          assert accion == Accion.SALIR;
          nuevoEstado = SALIENDO;
        }
        break;
      case FUERA:
      case SALIENDO:
        if ((accion == Accion.SALIR) || (accion == Accion.MANTENER)) {
          nuevoEstado = FUERA;
        } else {
          assert accion == Accion.ENTRAR;
          nuevoEstado = Estado.ENTRANDO;
        }
        break;
      default:
        throw new AssertionError("Acción desconocida = [" + accion + "]");
    }

    return nuevoEstado;
  }
}
