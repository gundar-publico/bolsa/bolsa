package com.gundar.bolsa.simulacion;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import com.gundar.bolsa.simulacion.EstadisticasSimulacion.EntradaSalida;

class MejoresRespuestas {

  private final int maximosValores;
  private final int maximasEntradasSalidas;
  private final int minimosDiasEntreCambios;

  private final List<ResultadoSimulacion> mejoresEstrategias;

  private final Comparator<ResultadoSimulacion> ordenMejoresResultados = ComparadorRendimientoEstrategias.INSTANCIA;

  public MejoresRespuestas(int maximosValores, int maximasEntradasSalidas, int minimosDiasEntreCambios) {
    this.maximosValores = maximosValores;
    this.maximasEntradasSalidas = maximasEntradasSalidas;
    this.minimosDiasEntreCambios = minimosDiasEntreCambios;
    this.mejoresEstrategias = new LinkedList<>();
  }

  public boolean anyadirMejores(ResultadoSimulacion nuevaEstrategia) {

    List<EntradaSalida> entradasSalidas = nuevaEstrategia.getEstadisticas().calcularEntradasSalidas();

    if (entradasSalidas.size() > maximasEntradasSalidas) {
      return false;
    }

    if (!cumpleDiasMinimosEntreCambios(entradasSalidas)) {
      return false;
    }

    if (mejoresEstrategias.isEmpty()) {
      mejoresEstrategias.add(nuevaEstrategia);
      return true;
    }

    /* Como las estrategias ya están ordenadas, la primera será la de ganancia más baja */
    ResultadoSimulacion peorEstrategiaGuardada = mejoresEstrategias.get(0);

    boolean nuevaEstrategiaMejor = ordenMejoresResultados.compare(nuevaEstrategia, peorEstrategiaGuardada) >= 0;

    boolean guardada = false;

    if (nuevaEstrategiaMejor) {

      if (mejoresEstrategias.size() >= maximosValores) {
        mejoresEstrategias.remove(0);
      }

      guardada = true;
      mejoresEstrategias.add(nuevaEstrategia);
      Collections.sort(mejoresEstrategias, ordenMejoresResultados);

    } else if (mejoresEstrategias.size() < maximosValores) {
      guardada = true;
      mejoresEstrategias.add(nuevaEstrategia);
      Collections.sort(mejoresEstrategias, ordenMejoresResultados);
    }

    return guardada;
  }

  private boolean cumpleDiasMinimosEntreCambios(List<EntradaSalida> entradasSalidas) {

    EntradaSalida entradaSalidaAnterior = null;

    for (EntradaSalida entradaSalida : entradasSalidas) {

      if (diferenciaDias(entradaSalida.getEntrada(), entradaSalida.getSalida()) < minimosDiasEntreCambios) {
        return false;
      }

      if ((entradaSalidaAnterior != null)
          && (diferenciaDias(entradaSalidaAnterior.getSalida(), entradaSalida.getEntrada()) < minimosDiasEntreCambios)) {
        return false;
      }

      entradaSalidaAnterior = entradaSalida;
    }

    return true;

  }

  private int diferenciaDias(LocalDate fecha1, LocalDate fecha2) {

    final int diferenciaDias;

    if ((fecha1 == null) || (fecha2 == null)) {
      diferenciaDias = Integer.MAX_VALUE;
    } else {
      diferenciaDias = (int) ChronoUnit.DAYS.between(fecha1, fecha2);
    }

    return Math.abs(diferenciaDias);
  }

  public List<ResultadoSimulacion> getEstrategias(Comparator<ResultadoSimulacion> comparator) {

    List<ResultadoSimulacion> resultadosOrdenados = new ArrayList<>(mejoresEstrategias);
    Collections.sort(resultadosOrdenados, comparator);

    return resultadosOrdenados;
  }

  public static class ComparadorRendimientoEstrategias implements Comparator<ResultadoSimulacion> {

    public static final ComparadorRendimientoEstrategias INSTANCIA = new ComparadorRendimientoEstrategias();

    @Override
    public int compare(ResultadoSimulacion o1, ResultadoSimulacion o2) {

      return Double.compare(o1.getAhorros().getCantidad(), o2.getAhorros().getCantidad());
    }

    @Override
    public String toString() {
      return "ComparadorRendimientoEstrategias []";
    }
  }

  public static class ComparadorNombreEstrategias implements Comparator<ResultadoSimulacion> {

    public static final ComparadorNombreEstrategias INSTANCIA = new ComparadorNombreEstrategias();

    @Override
    public int compare(ResultadoSimulacion o1, ResultadoSimulacion o2) {

      return o1.getNombre().compareToIgnoreCase(o2.getNombre());
    }

    @Override
    public String toString() {
      return "ComparadorNombreEstrategias []";
    }
  }

}
