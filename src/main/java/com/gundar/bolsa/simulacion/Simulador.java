package com.gundar.bolsa.simulacion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gundar.bolsa.Indices;
import com.gundar.bolsa.simulacion.configuracion.ConfiguracionSimulacion;

import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarStyle;

public class Simulador implements AutoCloseable {

  private static final Logger log = LoggerFactory.getLogger(Simulador.class);

  private final ExecutorService executor;

  public Simulador() {

    int numeroHilos = calcularNumeroHilos();
    // numeroHilos = 1;

    if (log.isInfoEnabled()) {
      log.info("Se usarán [" + numeroHilos + "] hilos en las simulaciones");
    }

    executor = Executors.newFixedThreadPool(numeroHilos);
  }

  private final int calcularNumeroHilos() {

    /* Dejamos un core libre para no bloquear la máquina */
    int numeroHilos = Runtime.getRuntime().availableProcessors() - 1;
    if (numeroHilos <= 0) {
      numeroHilos = 1;
    }

    return numeroHilos;
  }

  public Map<Rango, MejoresRespuestas> simula(List<ConfiguracionSimulacion> configuracionSimulaciones,
      Indices indices, List<Rango> rangos) {

    int numeroResultados = configuracionSimulaciones.size() * rangos.size();

    List<Future<ResultadoSimulacion>> resultados = new ArrayList<>(numeroResultados);

    for (ConfiguracionSimulacion configuracionSimulacion : configuracionSimulaciones) {

      for (Rango rango : rangos) {

        Future<ResultadoSimulacion> resultado = executor.submit(new SimulacionThread(configuracionSimulacion, rango, indices));

        resultados.add(resultado);
      }
    }

    Map<Rango, MejoresRespuestas> resultadosAgrupados = agruparResultadosPorRango(resultados);

    return resultadosAgrupados;

  }

  private Map<Rango, MejoresRespuestas> agruparResultadosPorRango(List<Future<ResultadoSimulacion>> resultados) {

    ProgressBar barraProgreso = new ProgressBar("Simulacion", resultados.size(), ProgressBarStyle.ASCII).start();

    try {

      final int maximosValores = 300;
      final int maximasEntradasSalidas = Integer.MAX_VALUE;
      final int diasMinimosEntreCambios = 0;

      Map<Rango, MejoresRespuestas> resultadosAgrupados = new TreeMap<>();

      for (Future<ResultadoSimulacion> resultadoFuture : resultados) {

        try {

          ResultadoSimulacion resultado = resultadoFuture.get();

          MejoresRespuestas mejoresResultadosPorRango = resultadosAgrupados.get(resultado.getRango());

          if (mejoresResultadosPorRango == null) {
            mejoresResultadosPorRango = new MejoresRespuestas(maximosValores, maximasEntradasSalidas, diasMinimosEntreCambios);
            resultadosAgrupados.put(resultado.getRango(), mejoresResultadosPorRango);
          }

          mejoresResultadosPorRango.anyadirMejores(resultado);

          barraProgreso.step();

        } catch (InterruptedException | ExecutionException e) {
          log.error("Error en el hilo de la estrategia", e);
        }
      }

      return resultadosAgrupados;

    } finally {
      barraProgreso.stop();
    }
  }

  @Override
  public void close() {

    if (executor != null) {
      executor.shutdownNow();
    }
  }

}
