package com.gundar.bolsa.simulacion;

import java.time.LocalDate;

public class Rango implements Comparable<Rango> {

  private LocalDate fechaInicial;
  private LocalDate fechaFinal;

  public Rango(LocalDate fechaInicial, LocalDate fechaFinal) {
    super();
    this.fechaInicial = (fechaInicial == null) ? LocalDate.MIN : fechaInicial;
    this.fechaFinal = (fechaFinal == null) ? LocalDate.MAX : fechaFinal;
  }

  public boolean fechaValida(LocalDate fecha) {

    boolean fechaInicialValida = !fecha.isBefore(fechaInicial);

    boolean fechaFinalValida = !fecha.isAfter(fechaFinal);

    return fechaInicialValida && fechaFinalValida;
  }

  public String pintarRango() {
    return "[" + fechaInicial + " - " + fechaFinal + "]";
  }

  @Override
  public int compareTo(Rango otroRango) {

    int iguales = fechaInicial.compareTo(otroRango.fechaInicial);

    if (iguales != 0) {
      return iguales;
    }

    return fechaFinal.compareTo(otroRango.fechaFinal);
  }

  @Override
  public String toString() {
    return "Rango [fechaInicial=" + fechaInicial + ", fechaFinal=" + fechaFinal + "]";
  }

  public LocalDate getFechaInicial() {
    return fechaInicial;
  }

  public LocalDate getFechaFinal() {
    return fechaFinal;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = (prime * result) + ((fechaFinal == null) ? 0 : fechaFinal.hashCode());
    result = (prime * result) + ((fechaInicial == null) ? 0 : fechaInicial.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Rango other = (Rango) obj;
    if (fechaFinal == null) {
      if (other.fechaFinal != null) {
        return false;
      }
    } else if (!fechaFinal.equals(other.fechaFinal)) {
      return false;
    }
    if (fechaInicial == null) {
      if (other.fechaInicial != null) {
        return false;
      }
    } else if (!fechaInicial.equals(other.fechaInicial)) {
      return false;
    }
    return true;
  }

}
