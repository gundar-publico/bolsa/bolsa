package com.gundar.bolsa.simulacion;

import com.gundar.bolsa.economia.ahorros.Ahorros;

public class ResultadoSimulacion {

  private final String nombre;
  private final boolean pintarEntradasSalidas;
  private final Rango rango;
  private final Ahorros ahorros;
  private final EstadisticasSimulacion estadisticas;

  public ResultadoSimulacion(String nombre, boolean pintarEntradasSalidas, Ahorros ahorros, EstadisticasSimulacion estadisticas) {
    this(nombre, pintarEntradasSalidas, null, ahorros, estadisticas);
  }

  public ResultadoSimulacion(String nombre, boolean pintarEntradasSalidas, Rango rango, Ahorros ahorros,
      EstadisticasSimulacion estadisticas) {
    super();
    this.nombre = nombre;
    this.pintarEntradasSalidas = pintarEntradasSalidas;
    this.rango = rango;
    this.ahorros = ahorros;
    this.estadisticas = estadisticas;
  }

  @Override
  public String toString() {
    return "ResultadoSimulacion [nombre=" + nombre + ", pintarEntradasSalidas=" + pintarEntradasSalidas + ", rango=" + rango + ", ahorros="
        + ahorros + ", estadisticas=" + estadisticas + "]";
  }

  public String getNombre() {
    return nombre;
  }

  public boolean isPintarEntradasSalidas() {
    return pintarEntradasSalidas;
  }

  public Rango getRango() {
    return rango;
  }

  public Ahorros getAhorros() {
    return ahorros;
  }

  public EstadisticasSimulacion getEstadisticas() {
    return estadisticas;
  }

}
