package com.gundar.bolsa.simulacion;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EstadisticasSimulacion {

  private List<LocalDate> entradas = new ArrayList<>();
  private List<LocalDate> salidas = new ArrayList<>();

  private int diasInvertido = 0;

  public void addEntrada(LocalDate fecha) {
    entradas.add(fecha);
  }

  public void addSalida(LocalDate fecha) {
    salidas.add(fecha);
  }

  public void addDiaInvertido() {
    diasInvertido++;
  }

  public List<EntradaSalida> calcularEntradasSalidas() {

    if (entradas.isEmpty() || salidas.isEmpty()) {
      return Collections.emptyList();
    }

    int indiceEntradas = 0;
    int indiceSalidas = 0;

    LocalDate primeraEntrada = entradas.get(indiceEntradas++);
    LocalDate primeraSalida = salidas.get(indiceSalidas++);

    /* Si la primera fecha es una salida, es que hemos emmpezado dentro
     * y no hay primera fecha de entrada */
    if (primeraSalida.isBefore(primeraEntrada)) {
      primeraEntrada = null;
      indiceEntradas--;
    }

    List<EntradaSalida> entradasSalidas = new ArrayList<>();
    entradasSalidas.add(new EntradaSalida(primeraEntrada, primeraSalida));

    while ((indiceEntradas < entradas.size()) || (indiceSalidas < salidas.size())) {

      LocalDate entrada = null;

      if (indiceEntradas < entradas.size()) {
        entrada = entradas.get(indiceEntradas++);
      }

      LocalDate salida = null;

      if (indiceSalidas < salidas.size()) {
        salida = salidas.get(indiceSalidas++);
      }

      entradasSalidas.add(new EntradaSalida(entrada, salida));
    }

    return entradasSalidas;
  }

  @Override
  public String toString() {
    return "EstadisticasSimulacion [entradas=" + entradas + ", salidas=" + salidas + ", diasInvertido=" + diasInvertido + "]";
  }

  public List<LocalDate> getEntradas() {
    return entradas;
  }

  public List<LocalDate> getSalidas() {
    return salidas;
  }

  public int getDiasInvertido() {
    return diasInvertido;
  }

  public static class EntradaSalida {

    private LocalDate entrada;
    private LocalDate salida;

    public EntradaSalida(LocalDate entrada, LocalDate salida) {
      super();
      this.entrada = entrada;
      this.salida = salida;
    }

    @Override
    public String toString() {
      return "EntradaSalida [entrada=" + entrada + ", salida=" + salida + "]";
    }

    public LocalDate getEntrada() {
      return entrada;
    }

    public LocalDate getSalida() {
      return salida;
    }
  }

}
