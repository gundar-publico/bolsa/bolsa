package com.gundar.bolsa.simulacion;

import java.time.LocalDate;

import com.gundar.bolsa.Indices.IndicesDia;
import com.gundar.bolsa.InformacionDia;
import com.gundar.bolsa.economia.EstrategiaEconomica;
import com.gundar.bolsa.economia.ahorros.Ahorros;
import com.gundar.bolsa.estrategias.EstrategiaMovimiento2;
import com.gundar.bolsa.estrategias.Filtro;
import com.gundar.bolsa.estrategias.ResultadoCiclo;
import com.gundar.bolsa.estrategias.ciclos.CalculoCiclo;

public class Simulacion {

  private final String nombre;
  private final EstrategiaMovimiento2 estrategiaMovimiento;
  private final CalculoCiclo calculoCiclo;

  private Estado estado;
  private EstadisticasSimulacion estadisticas = new EstadisticasSimulacion();
  private EstrategiaEconomica estrategiaEconomica;

  public Simulacion(String nombre, EstrategiaMovimiento2 estrategiaMovimiento, CalculoCiclo calculoCiclo,
      EstrategiaEconomica estrategiaEconomica) {
    this(nombre, estrategiaMovimiento, calculoCiclo, estrategiaEconomica, Estado.DENTRO);
  }

  public Simulacion(String nombre, EstrategiaMovimiento2 estrategiaMovimiento, CalculoCiclo calculoCiclo,
      EstrategiaEconomica estrategiaEconomica, Estado estadoInicial) {
    super();
    this.nombre = nombre;
    this.estrategiaMovimiento = estrategiaMovimiento;
    this.calculoCiclo = calculoCiclo;
    this.estrategiaEconomica = estrategiaEconomica;
    this.estado = estadoInicial;
  }

  public void calcular(IndicesDia indicesDia) {

    calcular(indicesDia, Filtro.FILTRO_VACIO);
  }

  public void calcular(IndicesDia indicesDia, Filtro filtro) {

    InformacionDia informacionDia = indicesDia.getIndicePrincipal();

    ResultadoCiclo resultadoCiclo = calculoCiclo.cambioCiclo(informacionDia);

    Accion accion = estrategiaMovimiento.accion(resultadoCiclo, filtro);

    boolean diaValido = filtro.cumpleFiltro(informacionDia.getFecha());

    if (diaValido) {

      estado = estrategiaEconomica.cambio(indicesDia, estado, accion);

      actualizaEstadisticas(informacionDia.getFecha());
    }

  }

  private void actualizaEstadisticas(LocalDate fecha) {

    if (estado == Estado.SALIENDO) {
      estadisticas.addSalida(fecha);
    } else if (estado == Estado.ENTRANDO) {
      estadisticas.addEntrada(fecha);
    }

    if (estado.enMercado()) {
      estadisticas.addDiaInvertido();
    }
  }

  // public ResultadoSimulacion getResultado() {
  // return new ResultadoSimulacion(nombre, getAhorros(), getEstadisticas());
  // }

  @Override
  public String toString() {
    return "Simulacion [estrategiaMovimiento=" + estrategiaMovimiento + ", calculoCiclo=" + calculoCiclo + ", estado=" + estado
        + ", estadisticas=" + estadisticas + ", estrategiaEconomica=" + estrategiaEconomica + "]";
  }

  public EstadisticasSimulacion getEstadisticas() {
    return estadisticas;
  }

  public Ahorros getAhorros() {
    return estrategiaEconomica.getAhorros();
  }
}
