package com.gundar.bolsa.simulacion;

import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gundar.bolsa.Indices;
import com.gundar.bolsa.Indices.IndicesDia;
import com.gundar.bolsa.economia.ahorros.Ahorros;
import com.gundar.bolsa.estrategias.Filtro;
import com.gundar.bolsa.simulacion.configuracion.ConfiguracionSimulacion;

public class SimulacionThread implements Callable<ResultadoSimulacion> {

  private static final Logger log = LoggerFactory.getLogger(SimulacionThread.class);

  private final ConfiguracionSimulacion configuracionSimulacion;
  private final Rango rango;
  private final Indices indices;

  public SimulacionThread(ConfiguracionSimulacion configuracionSimulacion, Rango rango, Indices indices) {
    super();
    this.configuracionSimulacion = configuracionSimulacion;
    this.rango = rango;
    this.indices = indices;
  }

  @Override
  public ResultadoSimulacion call() throws Exception {

    Thread.currentThread().setName(configuracionSimulacion.getNombre() + " , " + rango.pintarRango());

    if (log.isDebugEnabled()) {
      log.debug("Empezando simulacion");
    }

    Filtro filtro = new Filtro();
    filtro.setRangoFechas(rango);
    filtro.setPintarEntradasSalidas(false);

    Simulacion simulacion = crearSimulacion();

    for (IndicesDia indicesDia : indices) {

      simulacion.calcular(indicesDia, filtro);
    }

    return crearResultado(simulacion.getAhorros(), simulacion.getEstadisticas());
  }

  private Simulacion crearSimulacion() {

    return new Simulacion(
        configuracionSimulacion.getNombre(),
        configuracionSimulacion.getEstrategiaMovimiento().build(),
        configuracionSimulacion.getCalculoCiclo().build(),
        configuracionSimulacion.getEstrategiaEconomica().build());
  }

  private ResultadoSimulacion crearResultado(Ahorros ahorros, EstadisticasSimulacion estadisticas) {

    return new ResultadoSimulacion(
        configuracionSimulacion.getNombre(),
        configuracionSimulacion.isPintarEntradasSalidas(),
        rango,
        ahorros,
        estadisticas);
  }

  @Override
  public String toString() {
    return "SimulacionThread [configuracionSimulacion=" + configuracionSimulacion + ", rango=" + rango + ", indices=" + indices + "]";
  }

}
