package com.gundar.bolsa.simulacion;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gundar.bolsa.CargadorIbex35Csv;
import com.gundar.bolsa.Indices;
import com.gundar.bolsa.InformacionDia;
import com.gundar.bolsa.calculos.RentabilidadAnualizada;
import com.gundar.bolsa.economia.ahorros.Ahorros;
import com.gundar.bolsa.economia.impuestos.ImpuestosFijos;
import com.gundar.bolsa.economia.tarifas.TarifasDeGiro;
import com.gundar.bolsa.estrategias.ciclos.CicloDiario;
import com.gundar.bolsa.simulacion.EstadisticasSimulacion.EntradaSalida;
import com.gundar.bolsa.simulacion.MejoresRespuestas.ComparadorNombreEstrategias;
import com.gundar.bolsa.simulacion.MejoresRespuestas.ComparadorRendimientoEstrategias;
import com.gundar.bolsa.simulacion.configuracion.BuyAndHoldBuilder;
import com.gundar.bolsa.simulacion.configuracion.CalculoCicloLinealBuilder;
import com.gundar.bolsa.simulacion.configuracion.CalculoCicloMovilBuilder;
import com.gundar.bolsa.simulacion.configuracion.ConfiguracionAhorros;
import com.gundar.bolsa.simulacion.configuracion.ConfiguracionMacd;
import com.gundar.bolsa.simulacion.configuracion.ConfiguracionRsi;
import com.gundar.bolsa.simulacion.configuracion.ConfiguracionSimulacion;
import com.gundar.bolsa.simulacion.configuracion.EtfBuilder;
import com.gundar.bolsa.simulacion.configuracion.FondoInversionBuilder;
import com.gundar.bolsa.simulacion.configuracion.GastosBuilder;
import com.gundar.bolsa.simulacion.configuracion.MacdConRsiBuilder;
import com.gundar.bolsa.simulacion.configuracion.MomentoOperacionBuilder;

public class Test {

  private static final Logger log = LoggerFactory.getLogger(Test.class);

  private static ConfiguracionAhorros configuracionAhorros = new ConfiguracionAhorros(1000, 0);

  private static List<ConfiguracionSimulacion> crearEstrategiasEntrenamiento(List<InformacionDia> informacionDias) {

    boolean permiteInversos = true;

    List<ConfiguracionSimulacion> configuracionSimulaciones = new ArrayList<>();

    GastosBuilder gastosConImpuestosDeGiro = new GastosBuilder()
        .setTarifasBroker(new TarifasDeGiro())
        .setImpuestos(new ImpuestosFijos(0.21));

    final int umbralRsi = 50;
    for (int rsi = 1; rsi <= 14; rsi++) {
      // for (int umbralRsi = 40; umbralRsi <= 60; umbralRsi++) {
      for (int ciclo = 1; ciclo < 30; ciclo++) {
        for (int umbralMacdSubida = -20; umbralMacdSubida <= 20; umbralMacdSubida++) {
          for (int umbralMacdBajada = -20; umbralMacdBajada <= 20; umbralMacdBajada++) {

            String nombre = "macdConRsiMovil R = " + rsi + " UR = " + umbralRsi + " C = " + ciclo + " UMs = " + umbralMacdSubida + " UMb = "
                + umbralMacdBajada;

            MacdConRsiBuilder movimientoBuilder = new MacdConRsiBuilder()
                .setConfiguracionMacd(
                    new ConfiguracionMacd()
                        .setUmbralSubida(umbralMacdSubida)
                        .setUmbralBajada(umbralMacdBajada))
                .setConfiguracionRsi(new ConfiguracionRsi().setPeriodo(rsi))
                .setInformacionDias(informacionDias);

            CalculoCicloMovilBuilder calculoCicloBuilder = new CalculoCicloMovilBuilder(ciclo);

            EtfBuilder estrategiaEconomicaBuilder = new EtfBuilder()
                .setConfiguracionAhorros(configuracionAhorros)
                .setPermiteInverso(permiteInversos)
                .setGastos(gastosConImpuestosDeGiro)
                .setMomentoOperacion(MomentoOperacionBuilder.OPERAR_EN_APERTURA);

            ConfiguracionSimulacion configuracionSimulacion =
                new ConfiguracionSimulacion()
                    .setNombre(nombre)
                    .setPintarEntradasSalidas(false)
                    .setEstrategiaMovimiento(movimientoBuilder)
                    .setCalculoCiclo(calculoCicloBuilder)
                    .setEstrategiaEconomica(estrategiaEconomicaBuilder);

            configuracionSimulaciones.add(configuracionSimulacion);
          }
        }
      }
      // }
    }

    return configuracionSimulaciones;
  }

  private static List<ConfiguracionSimulacion> crearEstrategiasComparacion(List<InformacionDia> informacionDias) {

    GastosBuilder gastosConImpuestosDeGiro = new GastosBuilder()
        .setTarifasBroker(new TarifasDeGiro())
        .setImpuestos(new ImpuestosFijos(0.21));

    ConfiguracionMacd mejorConfiguracionMacd = new ConfiguracionMacd().setUmbralSubida(-20).setUmbralBajada(20);

    ConfiguracionRsi mejorConfiguracionRsi = new ConfiguracionRsi().setPeriodo(1);

    MomentoOperacionBuilder operacionEnCierre = MomentoOperacionBuilder.OPERAR_EN_CIERRE;
    MomentoOperacionBuilder operacionEnApertura = MomentoOperacionBuilder.OPERAR_EN_APERTURA;

    ConfiguracionSimulacion etf1 = new ConfiguracionSimulacion()
        .setNombre("etfCierre")
        .setEstrategiaMovimiento(
            new MacdConRsiBuilder()
                .setConfiguracionMacd(mejorConfiguracionMacd)
                .setConfiguracionRsi(mejorConfiguracionRsi)
                .setInformacionDias(informacionDias))
        .setCalculoCiclo(new CalculoCicloMovilBuilder(1))
        .setEstrategiaEconomica(
            new EtfBuilder()
                .setConfiguracionAhorros(configuracionAhorros)
                .setGastos(gastosConImpuestosDeGiro)
                .setMomentoOperacion(operacionEnCierre)
                .setPermiteInverso(true));

    ConfiguracionSimulacion etf2 = new ConfiguracionSimulacion()
        .setNombre("etfApertura")
        .setEstrategiaMovimiento(
            new MacdConRsiBuilder()
                .setConfiguracionMacd(mejorConfiguracionMacd)
                .setConfiguracionRsi(mejorConfiguracionRsi)
                .setInformacionDias(informacionDias))
        .setCalculoCiclo(new CalculoCicloMovilBuilder(1))
        .setEstrategiaEconomica(
            new EtfBuilder()
                .setConfiguracionAhorros(configuracionAhorros)
                .setGastos(gastosConImpuestosDeGiro)
                .setMomentoOperacion(operacionEnApertura)
                .setPermiteInverso(true));

    ConfiguracionSimulacion buyAndHold = new ConfiguracionSimulacion()
        .setNombre("BuyAndHold2")
        .setEstrategiaMovimiento(new BuyAndHoldBuilder("BuyAndHold2"))
        .setCalculoCiclo(new CalculoCicloLinealBuilder(new CicloDiario()))
        .setEstrategiaEconomica(
            new FondoInversionBuilder()
                .setConfiguracionAhorros(configuracionAhorros));

    List<ConfiguracionSimulacion> estrategiaMovimientos = Arrays.asList(
        etf1,
        etf2,
        buyAndHold);

    return estrategiaMovimientos;
  }

  public static void main(String[] args) throws Exception {

    List<InformacionDia> infoIndiceDirecto = null;
    List<InformacionDia> infoIndiceInverso = null;

    List<InformacionDia> informacionDias = new CargadorIbex35Csv().cargar();
    // List<InformacionDia> infoIndiceDirecto = new CargadorFR0011042753Csv().cargar();
    // List<InformacionDia> infoIndiceInverso = new CargadorFR0011036268Csv().cargar();
    // List<InformacionDia> infoIndiceInverso = new CargadorFR0010762492Csv().cargar();

    // List<InformacionDia> informacionDias = new CargadorSp500Csv().cargar();
    // List<InformacionDia> informacionDias = new CargadorDax30Csv().cargar();

    List<Rango> rangos = Arrays.asList(Rangos.getRangoCompleto(informacionDias));
    // List<Rango> rangos = Rangos.getRangosAgrupadosPorAnyos(informacionDias, 1);
    // List<Rango> rangos = Arrays.asList(Rangos.getRangoFiltrado(informacionDias, LocalDate.of(2012, 01, 01), LocalDate.of(2012, 12, 31)));
    // List<Rango> rangos = Rangos.getRangosAgrupadosPorAnyos(informacionDias, 1, LocalDate.of(2011, 05, 19), LocalDate.of(2017, 12, 31));

    Indices indices = new Indices(informacionDias, infoIndiceDirecto, infoIndiceInverso);

    // testEntrenamiento(indices, rangos);
    testComparacion(indices, rangos);

  }

  private static void testEntrenamiento(Indices indices, List<Rango> rangos) {

    List<ConfiguracionSimulacion> configuracionSimulaciones = crearEstrategiasEntrenamiento(indices.getIndicePrincipal());

    compararEstrategias(configuracionSimulaciones, indices, rangos, ComparadorRendimientoEstrategias.INSTANCIA);
  }

  private static void testComparacion(Indices indices, List<Rango> rangos) {

    List<ConfiguracionSimulacion> configuracionSimulaciones = crearEstrategiasComparacion(indices.getIndicePrincipal());

    compararEstrategias(configuracionSimulaciones, indices, rangos, ComparadorNombreEstrategias.INSTANCIA);
  }

  private static String formatearTiempos(long millis) {

    long horas = TimeUnit.MILLISECONDS.toHours(millis);
    millis = millis - TimeUnit.HOURS.toMillis(horas);

    long minutos = TimeUnit.MILLISECONDS.toMinutes(millis);
    millis = millis - TimeUnit.MINUTES.toMillis(minutos);

    long segundos = TimeUnit.MILLISECONDS.toSeconds(millis);
    millis = millis - TimeUnit.SECONDS.toMillis(segundos);

    String tiemposFormateados = "";

    if (horas > 0) {
      tiemposFormateados += " " + horas + " horas";
    }

    if (minutos > 0) {
      tiemposFormateados += " " + minutos + " minutos";
    }

    if (segundos > 0) {
      tiemposFormateados += " " + segundos + " segundos";
    }

    if (tiemposFormateados.isEmpty()) {
      tiemposFormateados = millis + " milisegundos";
    }

    return tiemposFormateados;
  }

  private static void compararEstrategias(List<ConfiguracionSimulacion> configuracionSimulaciones,
      Indices indices, List<Rango> rangos, Comparator<ResultadoSimulacion> ordenResultados) {

    try (Simulador simulador = new Simulador()) {

      Map<Rango, MejoresRespuestas> resultadosAgrupados = simulador.simula(configuracionSimulaciones, indices, rangos);

      for (Map.Entry<Rango, MejoresRespuestas> resultadosConRango : resultadosAgrupados.entrySet()) {

        Rango rango = resultadosConRango.getKey();

        MejoresRespuestas mejoresResultadosRango = resultadosConRango.getValue();

        System.err.println("Resultados entre " + rango.pintarRango());
        for (ResultadoSimulacion aplicacionEstrategia : mejoresResultadosRango.getEstrategias(ordenResultados)) {
          pintarResultados(aplicacionEstrategia, rango);
        }

        System.err.println();
      }
    }

  }

  private static void pintarResultados(ResultadoSimulacion aplicacionEstrategia, Rango rango) {

    Ahorros ahorros = aplicacionEstrategia.getAhorros();

    // pintarFechas(aplicacionEstrategia.getEstadisticas());

    if (aplicacionEstrategia.isPintarEntradasSalidas()) {
      System.err.println("Entradas salidas:");
      for (EntradaSalida entradaSalida : aplicacionEstrategia.getEstadisticas().calcularEntradasSalidas()) {
        System.err.println(entradaSalida.toString());
      }
    }

    int dias = (int) ChronoUnit.DAYS.between(rango.getFechaInicial(), rango.getFechaFinal());
    double anyos = redondear(dias / 365.0);

    System.err
        .println(aplicacionEstrategia.getNombre()
            + " ahorros invertidos = ["
            + ahorros.getCantidadInvertida() + "]"
            + " ahorros finales = ["
            + redondear(ahorros.getCantidad()) + "]"
            + " ganancia = [" + redondear(ahorros.calcularGananciaLineal()) + "]"
            + " ganancia % = [" + redondear(ahorros.calcularGananciaPorcentual() * 100) + "]"
            + " anualizada (" + anyos + ")  = ["
            + redondear(RentabilidadAnualizada.calculo(ahorros.getCantidadInvertida(), ahorros.getCantidad(), anyos) * 100) + "]"
            + " dias dentro = " + aplicacionEstrategia.getEstadisticas().getDiasInvertido()
            + " entradas salidas = " + aplicacionEstrategia.getEstadisticas().calcularEntradasSalidas().size());

  }

  private static double redondear(double value) {

    final int numeroDecimales = 2;

    BigDecimal valueBigDecimal = new BigDecimal(value).setScale(numeroDecimales, RoundingMode.HALF_DOWN);
    return valueBigDecimal.doubleValue();
  }

  private static void pintarFechas(EstadisticasSimulacion estadisticas) {

    List<LocalDate> entradas = estadisticas.getEntradas();

    List<LocalDate> salidas = estadisticas.getSalidas();

    if (entradas.isEmpty() || salidas.isEmpty()) {
      return;
    }

    int indiceEntradas = 0;
    int indiceSalidas = 0;

    // LocalDate primeraEntrada = entradas.get(indiceEntradas++);
    // LocalDate primeraSalida = salidas.get(indiceSalidas++);

    /* Si la primera fecha es una salida, es que hemos emmpezado dentro
     * y no hay primera fecha de entrada */
    // if (primeraSalida.isBefore(primeraEntrada)) {
    // primeraEntrada = null;
    // indiceEntradas--;
    // }

    while ((indiceEntradas < entradas.size()) || (indiceSalidas < salidas.size())) {

      LocalDate entrada = null;

      if (indiceEntradas < entradas.size()) {
        entrada = entradas.get(indiceEntradas++);
      }

      LocalDate salida = null;

      if (indiceSalidas < salidas.size()) {
        salida = salidas.get(indiceSalidas++);
      }

      if ((entrada != null) && (salida == null)) {

        System.err.println("Entra fecha = [" + entrada + "]");

      } else if ((entrada == null) && (salida != null)) {

        System.err.println("Sale fecha = [" + salida + "]");

      } else if ((entrada != null) && (salida != null)) {

        if (salida.isBefore(entrada)) {

          System.err.println("Sale fecha = [" + salida + "]");
          indiceEntradas--;

        } else {

          System.err.println("Entra fecha = [" + entrada + "]");
          indiceSalidas--;

        }

      }
    }

  }

}
