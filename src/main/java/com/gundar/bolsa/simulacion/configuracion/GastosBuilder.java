package com.gundar.bolsa.simulacion.configuracion;

import com.gundar.bolsa.economia.Gastos;
import com.gundar.bolsa.economia.GastosEtf;
import com.gundar.bolsa.economia.GastosFondoInversion;
import com.gundar.bolsa.economia.GastosImpuestosPlusvalias;
import com.gundar.bolsa.economia.GastosImpuestosPlusvalias2;
import com.gundar.bolsa.economia.impuestos.Impuestos;
import com.gundar.bolsa.economia.tarifas.TarifasBroker;

public class GastosBuilder {

  private TarifasBroker tarifasBroker;
  private Impuestos impuestos;

  public Gastos buildGastosEtf1() {

    if (tarifasBroker == null) {
      throw new IllegalStateException("Las tarifas del broker no pueden estar vacías");
    }

    Gastos gastosImpuestos = null;
    if (impuestos != null) {
      gastosImpuestos = new GastosImpuestosPlusvalias(null, impuestos);
    }

    return new GastosEtf(tarifasBroker, gastosImpuestos);
  }

  public Gastos buildGastosEtf2() {

    if (tarifasBroker == null) {
      throw new IllegalStateException("Las tarifas del broker no pueden estar vacías");
    }

    Gastos gastosImpuestos = null;
    if (impuestos != null) {
      gastosImpuestos = new GastosImpuestosPlusvalias2(null, impuestos);
    }

    return new GastosEtf(tarifasBroker, gastosImpuestos);
  }

  public Gastos buildGastosFondoInversion() {
    return new GastosFondoInversion();
  }

  public GastosBuilder setTarifasBroker(TarifasBroker tarifasBroker) {
    this.tarifasBroker = tarifasBroker;
    return this;
  }

  public GastosBuilder setImpuestos(Impuestos impuestos) {
    this.impuestos = impuestos;
    return this;
  }

}
