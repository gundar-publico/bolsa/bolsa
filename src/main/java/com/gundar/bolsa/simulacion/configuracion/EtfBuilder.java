package com.gundar.bolsa.simulacion.configuracion;

import com.gundar.bolsa.economia.CambioEstadoNormal;
import com.gundar.bolsa.economia.EstrategiaEconomica;
import com.gundar.bolsa.economia.EstrategiaEconomica2;
import com.gundar.bolsa.economia.Gastos;
import com.gundar.bolsa.economia.MomentoOperacion;
import com.gundar.bolsa.economia.ahorros.Ahorros;

public class EtfBuilder implements ConfiguracionBuilder<EstrategiaEconomica> {

  private ConfiguracionAhorros configuracionAhorros;
  private GastosBuilder gastosBuilder;
  private MomentoOperacionBuilder momentoOperacionBuilder;
  private boolean permiteInverso;

  /* 1 */
  // @Override
  // public EstrategiaEconomica build() {
  //
  // if (gastosBuilder == null) {
  // throw new IllegalStateException("Los gastos no pueden estar vacíos");
  // }
  //
  // Gastos gastos = gastosBuilder.buildGastosEtf1();
  //
  // if (momentoOperacionBuilder == null) {
  // throw new IllegalStateException("El momento de operación no pueden estar vacío");
  // }
  //
  // MomentoOperacion momentoOperacion = momentoOperacionBuilder.build(gastos);
  //
  // return EstrategiaEconomica1.etf(configuracionAhorros, gastos, momentoOperacion, permiteInverso);
  // }

  /* 2 */
  @Override
  public EstrategiaEconomica build() {

    if (gastosBuilder == null) {
      throw new IllegalStateException("Los gastos no pueden estar vacíos");
    }

    Ahorros ahorros = new Ahorros(configuracionAhorros);
    Gastos gastos = gastosBuilder.buildGastosEtf2();

    if (momentoOperacionBuilder == null) {
      throw new IllegalStateException("El momento de operación no pueden estar vacío");
    }

    MomentoOperacion momentoOperacion = momentoOperacionBuilder.build(gastos);

    return etf(ahorros, momentoOperacion, permiteInverso);
  }

  private static EstrategiaEconomica etf(Ahorros ahorros, MomentoOperacion momentoOperacion,
      boolean permiteInverso) {

    return new EstrategiaEconomica2(
        ahorros,
        new CambioEstadoNormal(),
        momentoOperacion,
        permiteInverso);
  }

  @Override
  public String toString() {
    return "EtfBuilder [configuracionAhorros=" + configuracionAhorros + ", gastosBuilder=" + gastosBuilder + ", momentoOperacionBuilder="
        + momentoOperacionBuilder + ", permiteInverso=" + permiteInverso + "]";
  }

  public EtfBuilder setConfiguracionAhorros(ConfiguracionAhorros configuracionAhorros) {
    this.configuracionAhorros = configuracionAhorros;
    return this;
  }

  public EtfBuilder setGastos(GastosBuilder gastosBuilder) {
    this.gastosBuilder = gastosBuilder;
    return this;
  }

  public EtfBuilder setMomentoOperacion(MomentoOperacionBuilder momentoOperacionBuilder) {
    this.momentoOperacionBuilder = momentoOperacionBuilder;
    return this;
  }

  public EtfBuilder setPermiteInverso(boolean permiteInverso) {
    this.permiteInverso = permiteInverso;
    return this;
  }

}
