package com.gundar.bolsa.simulacion.configuracion;

public interface ConfiguracionBuilder<T> {

  public T build();

}
