package com.gundar.bolsa.simulacion.configuracion;

import com.gundar.bolsa.economia.Gastos;
import com.gundar.bolsa.economia.MomentoOperacion;
import com.gundar.bolsa.economia.OperarConPrecioApertura;
import com.gundar.bolsa.economia.OperarConPrecioCierre;

public enum MomentoOperacionBuilder {

  OPERAR_EN_CIERRE {

    @Override
    public MomentoOperacion build(Gastos gastos) {
      return new OperarConPrecioCierre(gastos);
    }
  },
  OPERAR_EN_APERTURA {

    @Override
    public MomentoOperacion build(Gastos gastos) {
      return new OperarConPrecioApertura(gastos);
    }
  };

  public abstract MomentoOperacion build(Gastos gastos);

}
