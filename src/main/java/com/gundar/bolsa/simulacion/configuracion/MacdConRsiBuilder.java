package com.gundar.bolsa.simulacion.configuracion;

import java.util.List;

import com.gundar.bolsa.InformacionDia;
import com.gundar.bolsa.estrategias.EstrategiaMovimiento2;
import com.gundar.bolsa.estrategias.MacdConRsiGeneral2;

public class MacdConRsiBuilder implements ConfiguracionBuilder<EstrategiaMovimiento2> {

  private ConfiguracionMacd configuracionMacd;
  private ConfiguracionRsi configuracionRsi;

  private List<InformacionDia> informacionDias;

  @Override
  public EstrategiaMovimiento2 build() {
    return new MacdConRsiGeneral2(configuracionMacd, configuracionRsi, informacionDias);
  }

  @Override
  public String toString() {
    return "MacdConRsiBuilder [configuracionMacd=" + configuracionMacd + ", configuracionRsi=" + configuracionRsi + ", informacionDias="
        + informacionDias + "]";
  }

  public MacdConRsiBuilder setConfiguracionMacd(ConfiguracionMacd configuracionMacd) {
    this.configuracionMacd = configuracionMacd;
    return this;
  }

  public MacdConRsiBuilder setConfiguracionRsi(ConfiguracionRsi configuracionRsi) {
    this.configuracionRsi = configuracionRsi;
    return this;
  }

  public MacdConRsiBuilder setInformacionDias(List<InformacionDia> informacionDias) {
    this.informacionDias = informacionDias;
    return this;
  }

}
