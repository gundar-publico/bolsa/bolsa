package com.gundar.bolsa.simulacion.configuracion;

import com.gundar.bolsa.estrategias.ciclos.CalculoCiclo;
import com.gundar.bolsa.estrategias.ciclos.CalculoCicloMovil;

public class CalculoCicloMovilBuilder implements ConfiguracionBuilder<CalculoCiclo> {

  private final int diasCiclo;

  public CalculoCicloMovilBuilder(int diasCiclo) {
    super();
    this.diasCiclo = diasCiclo;
  }

  @Override
  public CalculoCiclo build() {
    return new CalculoCicloMovil(diasCiclo);
  }

  @Override
  public String toString() {
    return "CalculoCicloMovilBuilder [diasCiclo=" + diasCiclo + "]";
  }

}
