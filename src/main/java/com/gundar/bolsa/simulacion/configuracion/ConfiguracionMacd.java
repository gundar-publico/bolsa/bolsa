package com.gundar.bolsa.simulacion.configuracion;


public class ConfiguracionMacd {

  private static final int UMBRAL_POR_DEFECTO = 0;

  private int periodoCorto = 12;
  private int periodoLargo = 26;
  private int periodoSenal = 9;

  private int umbralSubida = UMBRAL_POR_DEFECTO;
  private int umbralBajada = UMBRAL_POR_DEFECTO;

  @Override
  public String toString() {
    return "ConfiguracionMacd [periodoCorto=" + periodoCorto + ", periodoLargo=" + periodoLargo + ", periodoSenal=" + periodoSenal
        + ", umbralSubida=" + umbralSubida + ", umbralBajada=" + umbralBajada + "]";
  }

  public int getPeriodoCorto() {
    return periodoCorto;
  }

  public int getPeriodoLargo() {
    return periodoLargo;
  }

  public int getPeriodoSenal() {
    return periodoSenal;
  }

  public int getUmbralSubida() {
    return umbralSubida;
  }

  public int getUmbralBajada() {
    return umbralBajada;
  }

  public ConfiguracionMacd setPeriodoCorto(int periodoCorto) {
    this.periodoCorto = periodoCorto;
    return this;
  }

  public ConfiguracionMacd setPeriodoLargo(int periodoLargo) {
    this.periodoLargo = periodoLargo;
    return this;
  }

  public ConfiguracionMacd setPeriodoSenal(int periodoSenal) {
    this.periodoSenal = periodoSenal;
    return this;
  }

  public ConfiguracionMacd setUmbralSubida(int umbralSubida) {
    this.umbralSubida = umbralSubida;
    return this;
  }

  public ConfiguracionMacd setUmbralBajada(int umbralBajada) {
    this.umbralBajada = umbralBajada;
    return this;
  }

}
