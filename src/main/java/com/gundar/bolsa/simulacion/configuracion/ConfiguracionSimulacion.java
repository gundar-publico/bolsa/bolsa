package com.gundar.bolsa.simulacion.configuracion;

import com.gundar.bolsa.economia.EstrategiaEconomica;
import com.gundar.bolsa.estrategias.EstrategiaMovimiento2;
import com.gundar.bolsa.estrategias.ciclos.CalculoCiclo;

public class ConfiguracionSimulacion {

  private String nombre;
  private boolean pintarEntradasSalidas;

  private ConfiguracionBuilder<EstrategiaMovimiento2> estrategiaMovimiento;
  private ConfiguracionBuilder<CalculoCiclo> calculoCiclo;
  private ConfiguracionBuilder<EstrategiaEconomica> estrategiaEconomica;

  @Override
  public String toString() {
    return "ConfiguracionSimulacion [nombre=" + nombre + ", pintarEntradasSalidas=" + pintarEntradasSalidas + ", estrategiaMovimiento="
        + estrategiaMovimiento + ", calculoCiclo=" + calculoCiclo + ", estrategiaEconomica=" + estrategiaEconomica + "]";
  }

  public String getNombre() {

    if (nombre == null) {
      return estrategiaMovimiento.getClass().getSimpleName();
    } else {
      return nombre;
    }
  }

  public boolean isPintarEntradasSalidas() {
    return pintarEntradasSalidas;
  }

  public ConfiguracionBuilder<EstrategiaMovimiento2> getEstrategiaMovimiento() {
    return estrategiaMovimiento;
  }

  public ConfiguracionBuilder<CalculoCiclo> getCalculoCiclo() {
    return calculoCiclo;
  }

  public ConfiguracionBuilder<EstrategiaEconomica> getEstrategiaEconomica() {
    return estrategiaEconomica;
  }

  public ConfiguracionSimulacion setNombre(String nombre) {
    this.nombre = nombre;
    return this;
  }

  public ConfiguracionSimulacion setEstrategiaMovimiento(ConfiguracionBuilder<EstrategiaMovimiento2> estrategiaMovimiento) {
    this.estrategiaMovimiento = estrategiaMovimiento;
    return this;
  }

  public ConfiguracionSimulacion setCalculoCiclo(ConfiguracionBuilder<CalculoCiclo> calculoCiclo) {
    this.calculoCiclo = calculoCiclo;
    return this;
  }

  public ConfiguracionSimulacion setEstrategiaEconomica(ConfiguracionBuilder<EstrategiaEconomica> estrategiaEconomica) {
    this.estrategiaEconomica = estrategiaEconomica;
    return this;
  }

  public ConfiguracionSimulacion setPintarEntradasSalidas(boolean pintarEntradasSalidas) {
    this.pintarEntradasSalidas = pintarEntradasSalidas;
    return this;
  }

}
