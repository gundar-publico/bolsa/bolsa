package com.gundar.bolsa.simulacion.configuracion;

import com.gundar.bolsa.estrategias.ciclos.CalculoCiclo;
import com.gundar.bolsa.estrategias.ciclos.CalculoCicloLineal;
import com.gundar.bolsa.estrategias.ciclos.Ciclo;

public class CalculoCicloLinealBuilder implements ConfiguracionBuilder<CalculoCiclo> {

  private final Ciclo ciclo;

  public CalculoCicloLinealBuilder(Ciclo diasCiclo) {
    super();
    this.ciclo = diasCiclo;
  }

  @Override
  public CalculoCiclo build() {
    return new CalculoCicloLineal(ciclo);
  }

  @Override
  public String toString() {
    return "CalculoCicloLinealBuilder [ciclo=" + ciclo + "]";
  }

}
