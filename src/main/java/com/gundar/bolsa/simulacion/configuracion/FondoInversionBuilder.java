package com.gundar.bolsa.simulacion.configuracion;

import com.gundar.bolsa.economia.CambioEstadoFondoInversion;
import com.gundar.bolsa.economia.CambioEstadoFondoInversion.OperativaFondo;
import com.gundar.bolsa.economia.ahorros.Ahorros;
import com.gundar.bolsa.economia.EstrategiaEconomica;
import com.gundar.bolsa.economia.EstrategiaEconomica2;
import com.gundar.bolsa.economia.Gastos;
import com.gundar.bolsa.economia.OperarConPrecioCierre;

public class FondoInversionBuilder implements ConfiguracionBuilder<EstrategiaEconomica> {

  private ConfiguracionAhorros configuracionAhorros;
  private boolean permiteInverso;

  @Override
  public EstrategiaEconomica build() {

    Ahorros ahorros = new Ahorros(configuracionAhorros);

    Gastos gastos = new GastosBuilder().buildGastosFondoInversion();

    // return EstrategiaEconomica1.fondoInversion(configuracionAhorros, permiteInverso);
    return fondoInversion(ahorros, gastos, permiteInverso);
  }

  private static EstrategiaEconomica fondoInversion(Ahorros ahorros, Gastos gastos, boolean permiteInverso) {

    return new EstrategiaEconomica2(
        ahorros,
        new CambioEstadoFondoInversion(new OperativaFondo(0, 1), new OperativaFondo(0, 1)),
        new OperarConPrecioCierre(gastos),
        permiteInverso);
  }

  @Override
  public String toString() {
    return "FondoInversionBuilder [configuracionAhorros=" + configuracionAhorros + "]";
  }

  public FondoInversionBuilder setConfiguracionAhorros(ConfiguracionAhorros configuracionAhorros) {
    this.configuracionAhorros = configuracionAhorros;
    return this;
  }

  public FondoInversionBuilder setPermiteInverso(boolean permiteInverso) {
    this.permiteInverso = permiteInverso;
    return this;
  }

}
