package com.gundar.bolsa.simulacion.configuracion;


public class ConfiguracionRsi {

  private static final int UMBRAL_POR_DEFECTO = 50;

  private int periodo = 14;
  private int umbral = UMBRAL_POR_DEFECTO;

  @Override
  public String toString() {
    return "ConfiguracionRsi [periodo=" + periodo + ", umbral=" + umbral + "]";
  }

  public int getPeriodo() {
    return periodo;
  }

  public int getUmbral() {
    return umbral;
  }

  public ConfiguracionRsi setPeriodo(int periodo) {
    this.periodo = periodo;
    return this;
  }

  public ConfiguracionRsi setUmbral(int umbral) {
    this.umbral = umbral;
    return this;
  }
}
