package com.gundar.bolsa.simulacion.configuracion;


public class ConfiguracionRsiAvanzado {

  public static final ConfiguracionRsiAvanzado ESTANDAR = new ConfiguracionRsiAvanzado(new ConfiguracionRsi(), 70, 1);

  private final ConfiguracionRsi configuracionRsi;
  private final int periodo;
  private final double multiplicadorBandas;

  public ConfiguracionRsiAvanzado(ConfiguracionRsi configuracionRsi, int periodo, double multiplicadorBandas) {
    super();
    this.configuracionRsi = configuracionRsi;
    this.periodo = periodo;
    this.multiplicadorBandas = multiplicadorBandas;
  }

  @Override
  public String toString() {
    return "ConfiguracionRsiAvanzado [configuracionRsi=" + configuracionRsi + ", periodo=" + periodo + ", multiplicadorBandas="
        + multiplicadorBandas + "]";
  }

  public ConfiguracionRsi getConfiguracionRsi() {
    return configuracionRsi;
  }

  public int getPeriodo() {
    return periodo;
  }

  public double getMultiplicadorBandas() {
    return multiplicadorBandas;
  }

}
