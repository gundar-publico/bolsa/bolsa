package com.gundar.bolsa.simulacion.configuracion;


public class ConfiguracionAhorros {

  private final double ingresoInicial;
  private final double ingresoMensual;

  public ConfiguracionAhorros(double ingresoInicial, double ingresoMensual) {
    super();
    this.ingresoInicial = ingresoInicial;
    this.ingresoMensual = ingresoMensual;
  }

  @Override
  public String toString() {
    return "ConfiguracionAhorros [ingresoInicial=" + ingresoInicial + ", ingresoMensual=" + ingresoMensual + "]";
  }

  public double getIngresoInicial() {
    return ingresoInicial;
  }

  public double getIngresoMensual() {
    return ingresoMensual;
  }

}
