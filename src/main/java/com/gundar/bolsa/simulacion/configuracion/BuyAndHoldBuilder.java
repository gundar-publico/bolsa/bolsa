package com.gundar.bolsa.simulacion.configuracion;

import com.gundar.bolsa.estrategias.BuyAndHold2;
import com.gundar.bolsa.estrategias.EstrategiaMovimiento2;

public class BuyAndHoldBuilder implements ConfiguracionBuilder<EstrategiaMovimiento2> {

  private final String nombre;

  public BuyAndHoldBuilder(String nombre) {
    super();
    this.nombre = nombre;
  }

  @Override
  public EstrategiaMovimiento2 build() {
    return new BuyAndHold2();
  }

  @Override
  public String toString() {
    return "BuyAndHoldBuilder [nombre=" + nombre + "]";
  }

}
