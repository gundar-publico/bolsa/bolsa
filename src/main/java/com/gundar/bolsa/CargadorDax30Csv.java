package com.gundar.bolsa;

import java.util.List;

public class CargadorDax30Csv {

  private static final String NOMBRE_ARCHIVO = "dax30.csv";

  private final CargadorCsv cargadorCsv;

  public CargadorDax30Csv() {
    this.cargadorCsv = new CargadorCsv(true);
  }

  public List<InformacionDia> cargar() {
    return cargadorCsv.cargar(NOMBRE_ARCHIVO);
  }

}
