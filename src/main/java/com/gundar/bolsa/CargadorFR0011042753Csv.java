package com.gundar.bolsa;

import java.util.List;

/**
 * Lyxor IBEX 35 Doble Apalancado Diario UCITS ETF - Acc
 */
public class CargadorFR0011042753Csv {

  private static final String NOMBRE_ARCHIVO = "FR0011042753.csv";

  private final CargadorCsv cargadorCsv;

  public CargadorFR0011042753Csv() {
    this.cargadorCsv = new CargadorCsv(true);
  }

  public List<InformacionDia> cargar() {
    return cargadorCsv.cargar(NOMBRE_ARCHIVO);
  }

}
