package com.gundar.bolsa.estrategias;

import com.gundar.bolsa.simulacion.Accion;

public class BuyAndHold2 implements EstrategiaMovimiento2 {

  @Override
  public Accion accion(ResultadoCiclo resultadoCiclo) {

    return accion(resultadoCiclo, Filtro.FILTRO_VACIO);
  }

  @Override
  public Accion accion(ResultadoCiclo resultadoCiclo, Filtro filtro) {
    return Accion.ENTRAR;
  }

}
