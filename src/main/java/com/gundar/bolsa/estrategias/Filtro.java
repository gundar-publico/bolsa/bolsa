package com.gundar.bolsa.estrategias;

import java.time.LocalDate;

import com.gundar.bolsa.simulacion.Rango;

public class Filtro {

  public static final Filtro FILTRO_VACIO = new Filtro();

  private Rango rangoFechas;
  private boolean pintarEntradasSalidas;

  public boolean cumpleFiltro(LocalDate fecha) {

    return cumpleFechas(fecha);
  }

  private boolean cumpleFechas(LocalDate fecha) {

    boolean cumpleFechas = true;

    if (rangoFechas != null) {
      cumpleFechas = rangoFechas.fechaValida(fecha);
    }

    return cumpleFechas;
  }

  @Override
  public String toString() {
    return "Filtro [rangoFechas=" + rangoFechas + ", pintarEntradasSalidas=" + pintarEntradasSalidas + "]";
  }

  public void setRangoFechas(Rango rangoFechas) {
    this.rangoFechas = rangoFechas;
  }

  public void setPintarEntradasSalidas(boolean pintarEntradasSalidas) {
    this.pintarEntradasSalidas = pintarEntradasSalidas;
  }

  public boolean isPintarEntradasSalidas() {
    return pintarEntradasSalidas;
  }

}
