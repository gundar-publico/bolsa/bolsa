package com.gundar.bolsa.estrategias.ciclos;

import com.gundar.bolsa.InformacionDia;
import com.gundar.bolsa.estrategias.ResultadoCiclo;

public class CalculoCicloLineal implements CalculoCiclo {

  private InformacionDia informacionInicioCiclo;
  private InformacionDia informacionFinCiclo;

  private final Ciclo tipoCiclo;

  public CalculoCicloLineal(Ciclo tipoCiclo) {
    this.tipoCiclo = tipoCiclo;
  }

  @Override
  public ResultadoCiclo cambioCiclo(InformacionDia informacionDia) {

    final ResultadoCiclo resultadoCiclo;

    if (informacionInicioCiclo == null) {
      resultadoCiclo = ResultadoCiclo.CICLO_ACTUAL;
      informacionInicioCiclo = informacionDia;
      informacionFinCiclo = informacionDia;
    } else {

      int cicloFechaInicio = tipoCiclo.extraerCiclo(informacionInicioCiclo.getFecha());
      int cicloFechaActual = tipoCiclo.extraerCiclo(informacionDia.getFecha());

      if (cicloFechaInicio == cicloFechaActual) {

        resultadoCiclo = ResultadoCiclo.CICLO_ACTUAL;
        informacionFinCiclo = informacionDia;

      } else {

        resultadoCiclo = ResultadoCiclo.crear(informacionInicioCiclo, informacionFinCiclo);
        informacionInicioCiclo = informacionDia;
        informacionFinCiclo = informacionDia;
      }
    }

    return resultadoCiclo;
  }

}
