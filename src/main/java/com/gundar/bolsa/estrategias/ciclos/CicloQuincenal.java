package com.gundar.bolsa.estrategias.ciclos;

import java.time.LocalDate;

public class CicloQuincenal implements Ciclo {

  private static final int DIA_SEPARADOR_QUINCENAS = 15;

  @Override
  public int extraerCiclo(LocalDate fecha) {

    int quincena = extraerQuincena(fecha);

    return quincena;
  }

  private int extraerQuincena(LocalDate fecha) {

    int diaDelMes = fecha.getDayOfMonth();

    final int quincena;

    if (diaDelMes <= DIA_SEPARADOR_QUINCENAS) {
      quincena = 1;
    } else {
      quincena = 2;
    }

    return quincena;
  }

}