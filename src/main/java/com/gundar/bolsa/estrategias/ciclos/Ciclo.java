package com.gundar.bolsa.estrategias.ciclos;

import java.time.LocalDate;

public interface Ciclo {

  public int extraerCiclo(LocalDate fecha);

}
