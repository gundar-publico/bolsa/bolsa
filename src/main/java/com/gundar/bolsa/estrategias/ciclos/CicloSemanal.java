package com.gundar.bolsa.estrategias.ciclos;

import java.time.LocalDate;
import java.time.temporal.WeekFields;

public class CicloSemanal implements Ciclo {

  @Override
  public int extraerCiclo(LocalDate fecha) {

    return fecha.get(WeekFields.ISO.weekOfWeekBasedYear());
  }

}