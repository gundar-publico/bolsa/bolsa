package com.gundar.bolsa.estrategias.ciclos;

import com.gundar.bolsa.InformacionDia;
import com.gundar.bolsa.estrategias.ResultadoCiclo;

public interface CalculoCiclo {

  public ResultadoCiclo cambioCiclo(InformacionDia informacionDia);

}
