package com.gundar.bolsa.estrategias.ciclos;

import com.gundar.bolsa.InformacionDia;
import com.gundar.bolsa.estrategias.ResultadoCiclo;

public class CalculoCicloMovil implements CalculoCiclo {

  // private final Deque<InformacionDia> informacionesNuevas;

  private InformacionDia[] informaciones;

  public CalculoCicloMovil(int ciclo) {
    informaciones = new InformacionDia[ciclo];
    // informacionesNuevas = new LinkedList<InformacionDia>(ciclo);
  }

  @Override
  public ResultadoCiclo cambioCiclo(InformacionDia informacionDia) {

    InformacionDia informacionInicioCiclo = pop();

    final ResultadoCiclo resultadoCiclo;

    if (informacionInicioCiclo == null) {

      resultadoCiclo = ResultadoCiclo.CICLO_ACTUAL;

    } else {

      InformacionDia informacionFinCiclo = informacionDia;

      resultadoCiclo = ResultadoCiclo.crear(informacionInicioCiclo, informacionFinCiclo);
    }

    push(informacionDia);

    return resultadoCiclo;
  }

  private void push(InformacionDia nuevaInformacion) {

    /* Desplaza todos los elementos una posición hacia delante */
    for (int i = informaciones.length - 1; i > 0; i--) {

      informaciones[i] = informaciones[i - 1];
    }

    /* Inserta el nuevo elemento al principio del array */
    informaciones[0] = nuevaInformacion;
  }

  private InformacionDia pop() {

    /* Devuelve el último elemento */
    return informaciones[informaciones.length - 1];
  }

}
