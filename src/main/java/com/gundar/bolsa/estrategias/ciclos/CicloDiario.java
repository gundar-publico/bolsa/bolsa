package com.gundar.bolsa.estrategias.ciclos;

import java.time.LocalDate;

public class CicloDiario implements Ciclo {

  @Override
  public int extraerCiclo(LocalDate fecha) {

    return fecha.getDayOfYear();
  }

}