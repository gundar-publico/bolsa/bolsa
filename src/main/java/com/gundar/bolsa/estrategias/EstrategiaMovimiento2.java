package com.gundar.bolsa.estrategias;

import com.gundar.bolsa.simulacion.Accion;

public interface EstrategiaMovimiento2 {

  public Accion accion(ResultadoCiclo resultadoCiclo);

  public Accion accion(ResultadoCiclo resultadoCiclo, Filtro filtro);

}
