package com.gundar.bolsa.estrategias;

import java.time.LocalDate;

import com.gundar.bolsa.InformacionDia;

public class ResultadoCiclo {

  public static ResultadoCiclo CICLO_ACTUAL = new ResultadoCiclo(null, -1, null, -1, -1, -1, false);

  private LocalDate fechaInicial;
  private double precioInicial;

  private LocalDate fechaFinal;
  private double precioFinal;

  private double diferenciaLineal;
  private double diferenciaPorcentual = -1;

  private boolean finalizado;

  public ResultadoCiclo(LocalDate fechaInicial, double precioInicial, LocalDate fechaFinal, double precioFinal,
      double diferenciaLineal, double diferenciaPorcentual, boolean finalizado) {
    super();
    this.fechaInicial = fechaInicial;
    this.precioInicial = precioInicial;
    this.fechaFinal = fechaFinal;
    this.precioFinal = precioFinal;
    this.diferenciaLineal = diferenciaLineal;
    this.diferenciaPorcentual = diferenciaPorcentual;
    this.finalizado = finalizado;
  }

  public static final ResultadoCiclo crear(InformacionDia informacionInicioCiclo, InformacionDia informacionFinCiclo) {

    double precioInicial = informacionInicioCiclo.getPrecioCierre() - informacionInicioCiclo.getDiferenciaLineal();

    double acumuladoLineal = informacionFinCiclo.getPrecioCierre() - precioInicial;

    double acumuladoPorcentual = acumuladoLineal / precioInicial;

    return new ResultadoCiclo(
        informacionInicioCiclo.getFecha(),
        precioInicial,
        informacionFinCiclo.getFecha(),
        informacionFinCiclo.getPrecioCierre(),
        acumuladoLineal, acumuladoPorcentual,
        true);
  }

  public String getFechasCiclo() {
    return fechaInicial + " - " + fechaFinal;
  }

  @Override
  public String toString() {
    return "ResultadoCiclo [fechaInicial=" + fechaInicial + ", precioInicial=" + precioInicial + ", fechaFinal=" + fechaFinal
        + ", precioFinal=" + precioFinal + ", diferenciaLineal=" + diferenciaLineal + ", diferenciaPorcentual=" + diferenciaPorcentual
        + ", finalizado=" + finalizado + "]";
  }

  public double getDiferenciaLineal() {
    return diferenciaLineal;
  }

  public double getDiferenciaPorcentual() {
    return diferenciaPorcentual;
  }

  public boolean isFinalizado() {
    return finalizado;
  }

  public double getPrecioInicial() {
    return precioInicial;
  }

  public double getPrecioFinal() {
    return precioFinal;
  }

  public LocalDate getFechaInicial() {
    return fechaInicial;
  }

  public LocalDate getFechaFinal() {
    return fechaFinal;
  }
}
