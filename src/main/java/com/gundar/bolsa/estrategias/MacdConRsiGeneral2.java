package com.gundar.bolsa.estrategias;

import java.util.Arrays;
import java.util.Collection;
import java.util.Deque;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gundar.bolsa.InformacionDia;
import com.gundar.bolsa.calculos.Macd;
import com.gundar.bolsa.calculos.Rsi;
import com.gundar.bolsa.simulacion.Accion;
import com.gundar.bolsa.simulacion.configuracion.ConfiguracionMacd;
import com.gundar.bolsa.simulacion.configuracion.ConfiguracionRsi;

public class MacdConRsiGeneral2 implements EstrategiaMovimiento2 {

  private static final Logger log = LoggerFactory.getLogger(MacdConRsiGeneral2.class);

  private final ConfiguracionMacd configuracionMacd;
  private final ConfiguracionRsi configuracionRsi;

  private Deque<Indicadores> indicadoresCiclosAnteriores;

  private final Indicadores primerosIndicadores;

  public MacdConRsiGeneral2(ConfiguracionMacd configuracionMacd, ConfiguracionRsi configuracionRsi,
      Collection<InformacionDia> informacionDias) {
    super();
    this.configuracionMacd = configuracionMacd;
    this.configuracionRsi = configuracionRsi;

    this.primerosIndicadores = crearIndicadoresIniciales(informacionDias);
    this.indicadoresCiclosAnteriores = new LinkedList<>(Arrays.asList(primerosIndicadores));
  }

  private final Indicadores crearIndicadoresIniciales(Collection<InformacionDia> informacionDias) {

    Macd macd = new Macd(informacionDias, configuracionMacd);

    Rsi rsi = new Rsi(informacionDias, configuracionRsi);

    return new Indicadores(macd, rsi);
  }

  @Override
  public Accion accion(ResultadoCiclo resultadoCiclo) {

    return accion(resultadoCiclo, Filtro.FILTRO_VACIO);
  }

  @Override
  public Accion accion(ResultadoCiclo resultadoCiclo, Filtro filtro) {

    /* Si no hay cambio de ciclo mantememos la posición que tengamos */
    if (!resultadoCiclo.isFinalizado()) {
      return Accion.MANTENER;
    }

    Indicadores indicadoresCicloAnterior = getIndicadoresCicloAnterior();
    Indicadores indicadoresActuales = crearInidicadoresCiclo(resultadoCiclo, indicadoresCicloAnterior);

    anyadirIndicadores(indicadoresActuales);

    if (log.isDebugEnabled()) {

      if (filtro.cumpleFiltro(resultadoCiclo.getFechaFinal())) {
        log.debug("Fechas = [" + resultadoCiclo.getFechasCiclo() + "] valor = [" + resultadoCiclo.getPrecioFinal() + "] "
            + indicadoresActuales.descripcion());
      }
    }

    Accion accion = decidirAccion(indicadoresActuales);

    return accion;
  }

  private Indicadores crearInidicadoresCiclo(ResultadoCiclo resultadoCiclo, Indicadores indicadoresAnteriores) {

    double ultimoPrecio = resultadoCiclo.getPrecioFinal();
    double diferenciaLineal = resultadoCiclo.getDiferenciaLineal();

    Macd macdActual = new Macd(configuracionMacd);
    macdActual.calcular(indicadoresAnteriores.macd, ultimoPrecio);

    Rsi rsiActual = new Rsi(configuracionRsi);
    rsiActual.calcular(indicadoresAnteriores.rsi, diferenciaLineal);

    return new Indicadores(macdActual, rsiActual);
  }

  private Accion decidirAccion(Indicadores indicadores) {

    double histograma = indicadores.macd.getHistograma();
    double valorRsi = indicadores.rsi.getValor();

    final Accion accion;

    if ((histograma > configuracionMacd.getUmbralSubida()) && (valorRsi > configuracionRsi.getUmbral())) {
      accion = Accion.ENTRAR;
    } else if ((histograma < configuracionMacd.getUmbralBajada()) && (valorRsi < configuracionRsi.getUmbral())) {
      accion = Accion.SALIR;
    } else {
      accion = Accion.MANTENER;
    }

    return accion;
  }

  private void anyadirIndicadores(Indicadores indicadores) {

    indicadoresCiclosAnteriores.addFirst(indicadores);
  }

  private Indicadores getIndicadoresCicloAnterior() {

    return indicadoresCiclosAnteriores.peekFirst();
  }

  private static class Indicadores {

    private Macd macd;
    private Rsi rsi;

    public Indicadores(Macd macd, Rsi rsi) {
      super();
      this.macd = macd;
      this.rsi = rsi;
    }

    public String descripcion() {

      return "Macd = [" + macd.getValorMacd() + "] Rsi = [" + rsi.getValor() + "]";
    }

  }

}
