package com.gundar.bolsa;

import java.util.List;

public class CargadorSp500Csv {

  private static final String NOMBRE_ARCHIVO = "sp500.csv";

  private final CargadorCsv cargadorCsv;

  public CargadorSp500Csv() {
    this.cargadorCsv = new CargadorCsv(true);
  }

  public List<InformacionDia> cargar() {
    return cargadorCsv.cargar(NOMBRE_ARCHIVO);
  }

}
