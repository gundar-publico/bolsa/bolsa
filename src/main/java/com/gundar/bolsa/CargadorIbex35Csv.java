package com.gundar.bolsa;

import java.util.List;

public class CargadorIbex35Csv {

  private static final String NOMBRE_ARCHIVO = "ibex35.csv";

  private final CargadorCsv cargadorCsv;

  public CargadorIbex35Csv() {
    this.cargadorCsv = new CargadorCsv(true);
  }

  public List<InformacionDia> cargar() {
    return cargadorCsv.cargar(NOMBRE_ARCHIVO);
  }

}
