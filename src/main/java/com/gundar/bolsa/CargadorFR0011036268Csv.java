package com.gundar.bolsa;

import java.util.List;

/**
 * Lyxor IBEX 35 Doble Inverso Diario UCITS ETF - Acc
 */
public class CargadorFR0011036268Csv {

  private static final String NOMBRE_ARCHIVO = "FR0011036268.csv";

  private final CargadorCsv cargadorCsv;

  public CargadorFR0011036268Csv() {
    this.cargadorCsv = new CargadorCsv(true);
  }

  public List<InformacionDia> cargar() {
    return cargadorCsv.cargar(NOMBRE_ARCHIVO);
  }

}
