package com.gundar.bolsa;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CargadorCsv {

  private static final Logger log = LoggerFactory.getLogger(CargadorCsv.class);
  private static final String RUTA_BASE = "/home/gun/workspace_oxygen/bolsa/src/main/resources/datos/";

  private static final int CABECERA_FECHA = 0;
  private static final int CABECERA_PRECIO = 1;
  private static final int CABECERA_PRECIO_APERTURA = 2;
  private static final int CABECERA_DIFERENCIA = 3;
  private static final int CABECERA_DIFERENCIA_PORCENTUAL = 4;

  private final boolean primeraLineaCabecera;

  public CargadorCsv(boolean primeraLineaCabecera) {
    this.primeraLineaCabecera = primeraLineaCabecera;
  }

  public List<InformacionDia> cargar(String nombreArchivo) {

    String rutaArchivo = RUTA_BASE + nombreArchivo;

    Iterable<CSVRecord> valoresCsv = cargarRegistros(rutaArchivo);

    List<InformacionDia> informacionDias = new ArrayList<>();

    for (CSVRecord filaCsv : valoresCsv) {

      InformacionDia informacionDia = convertirRegistro(filaCsv);

      informacionDias.add(informacionDia);
    }

    return informacionDias;
  }

  private InformacionDia convertirRegistro(CSVRecord record) {

    String fechaCsv = record.get(CABECERA_FECHA);
    LocalDate fecha = LocalDate.parse(fechaCsv);

    String precioCsv = record.get(CABECERA_PRECIO);
    double precio = Double.parseDouble(precioCsv);

    String precioAperturaCsv = record.get(CABECERA_PRECIO_APERTURA);
    Double precioApertura = null;

    if ((precioAperturaCsv != null) && !precioAperturaCsv.isEmpty()) {

      precioApertura = Double.parseDouble(precioAperturaCsv);

      if (Double.compare(precio, precioApertura) == 0) {

        /* Si el precio de apertura y cierre son iguales casi seguro que es un error en los datos */
        precioApertura = null;

        if (log.isWarnEnabled()) {
          log.warn("Para la fecha = [" + fecha + "] precio de apertura y cierre iguales = [" + precio + "]");
        }
      }
    }

    String diferenciaCsv = record.get(CABECERA_DIFERENCIA);
    double diferenciaLineal = Double.parseDouble(diferenciaCsv);

    String diferenciaPorcentualCsv = record.get(CABECERA_DIFERENCIA_PORCENTUAL);
    double diferenciaPorcentual = Double.parseDouble(diferenciaPorcentualCsv);

    return new InformacionDia(fecha, precio, precioApertura, diferenciaPorcentual, diferenciaLineal);

  }

  private Iterable<CSVRecord> cargarRegistros(String rutaArchivo) {

    try {

      Reader in = new FileReader(rutaArchivo);

      CSVFormat parserCsv = CSVFormat.DEFAULT;

      if (primeraLineaCabecera) {
        parserCsv = parserCsv.withFirstRecordAsHeader();
      }

      Iterable<CSVRecord> records = parserCsv.parse(in);

      return records;

    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

}
