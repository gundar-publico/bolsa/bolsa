package com.gundar.bolsa.economia.tarifas;

import java.util.Arrays;
import java.util.Collection;

public class TarifasDeGiro extends TarifasBrokerGenerica {

  private static final double COSTE_MINIMO = 0;
  private static final double COSTE_MAXIMO = 10;

  public TarifasDeGiro() {
    super(crearTramos());
  }

  private static Collection<TramoOperacion> crearTramos() {

    Collection<TramoOperacion> tramosImpuestos = Arrays.asList(
        new TramoOperacion(0, Integer.MAX_VALUE, 2, 0.0002, COSTE_MINIMO, COSTE_MAXIMO));

    return tramosImpuestos;
  }

}
