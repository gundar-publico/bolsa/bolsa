package com.gundar.bolsa.economia.tarifas;

import com.gundar.bolsa.economia.ahorros.Ahorros;

public class TarifasDeGiroViejo implements TarifasBroker {

  @Override
  public void cambio(Ahorros ahorros) {

    double gastos = calcularGastos(ahorros.getCantidad());
    double nuevaCantidad = ahorros.getCantidad() - gastos;

    ahorros.setCantidad(nuevaCantidad);
  }

  @Override
  public double calcularGastos(double cantidad) {

    return 2 + (cantidad * 0.0002);
  }

}
