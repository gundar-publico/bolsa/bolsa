package com.gundar.bolsa.economia.tarifas;

import com.gundar.bolsa.economia.ahorros.CambioAhorros;

public interface TarifasBroker extends CambioAhorros {

  public double calcularGastos(double cantidad);

}
