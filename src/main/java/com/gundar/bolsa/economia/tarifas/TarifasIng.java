package com.gundar.bolsa.economia.tarifas;

import java.util.Arrays;
import java.util.Collection;

public class TarifasIng extends TarifasBrokerGenerica {

  public TarifasIng() {
    super(crearTramos());
  }

  private static Collection<TramoOperacion> crearTramos() {

    Collection<TramoOperacion> tramosImpuestos = Arrays.asList(
        TramoOperacion.costeLineal(0, 30_000, 8),
        TramoOperacion.costePorcentual(30_000, Integer.MAX_VALUE, 0.002));

    return tramosImpuestos;
  }

}
