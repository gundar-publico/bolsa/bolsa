package com.gundar.bolsa.economia.tarifas;

import java.util.Arrays;
import java.util.Collection;

public class TarifasBnp extends TarifasBrokerGenerica {

  public TarifasBnp() {
    super(crearTramos());
  }

  private static Collection<TramoOperacion> crearTramos() {

    Collection<TramoOperacion> tramosImpuestos = Arrays.asList(
        TramoOperacion.costeLineal(0, 1_000, 2.75),
        TramoOperacion.costeLineal(1_000, 2_500, 4.75),
        TramoOperacion.costeLineal(2_500, 25_000, 9.75),
        TramoOperacion.costePorcentual(25_000, Integer.MAX_VALUE, 0.001));

    return tramosImpuestos;
  }

}
