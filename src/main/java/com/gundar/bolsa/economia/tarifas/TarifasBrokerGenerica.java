package com.gundar.bolsa.economia.tarifas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import com.gundar.bolsa.economia.ahorros.Ahorros;

public class TarifasBrokerGenerica implements TarifasBroker {

  private final List<TramoOperacion> tramosOperaciones;

  public TarifasBrokerGenerica(Collection<TramoOperacion> tramosOperaciones) {
    super();
    this.tramosOperaciones = new ArrayList<>(tramosOperaciones);
  }

  public TarifasBrokerGenerica() {
    this(crearTramos());
  }

  private static Collection<TramoOperacion> crearTramos() {

    Collection<TramoOperacion> tramosImpuestos = Arrays.asList(
        TramoOperacion.costeLineal(0, 1_000, 2.75),
        TramoOperacion.costeLineal(1_000, 2_500, 4.75),
        TramoOperacion.costeLineal(2_500, 25_000, 9.75),
        TramoOperacion.costePorcentual(25_000, Integer.MAX_VALUE, 0.001));

    return tramosImpuestos;
  }

  @Override
  public void cambio(Ahorros ahorros) {

    double gastos = calcularGastos(ahorros.getCantidad());
    double nuevaCantidad = ahorros.getCantidad() - gastos;

    ahorros.setCantidad(nuevaCantidad);
  }

  @Override
  public double calcularGastos(double cantidad) {

    double costeOperacion = 0;

    for (TramoOperacion tramoOperacion : tramosOperaciones) {

      costeOperacion += tramoOperacion.calcula(cantidad);
    }

    return costeOperacion;
  }

  @Override
  public String toString() {
    return "TarifasBrokerGenerica [tramosOperaciones=" + tramosOperaciones + "]";
  }

  protected static class TramoOperacion {

    private final int limiteInferior;
    private final int limiteSuperior;

    private final double costeLineal;
    private final double costePorcentual;

    private final double costeMinimo;
    private final double costeMaximo;

    public TramoOperacion(int limiteInferior, int limiteSuperior, double costeLineal, double costePorcentual,
        double costeMinimo, double costeMaximo) {
      super();
      this.limiteInferior = limiteInferior;
      this.limiteSuperior = limiteSuperior;
      this.costeLineal = costeLineal;
      this.costePorcentual = costePorcentual;
      this.costeMinimo = costeMinimo;
      this.costeMaximo = costeMaximo;
    }

    public static TramoOperacion costeLineal(int limiteInferior, int limiteSuperior, double costeLineal) {
      return new TramoOperacion(limiteInferior, limiteSuperior, costeLineal, 0, 0, Double.MAX_VALUE);
    }

    public static TramoOperacion costePorcentual(int limiteInferior, int limiteSuperior, double costePorcentual) {
      return new TramoOperacion(limiteInferior, limiteSuperior, 0, costePorcentual, 0, Double.MAX_VALUE);
    }

    public double calcula(double cantidad) {

      double coste = 0;

      /* limiteInferior < cantidad <= limiteSuperior */
      if ((cantidad > limiteInferior) && (cantidad <= limiteSuperior)) {
        coste = (cantidad * costePorcentual) + costeLineal;
      }

      if (coste < costeMinimo) {
        coste = costeMinimo;
      } else if (coste > costeMaximo) {
        coste = costeMaximo;
      }

      return coste;
    }

    @Override
    public String toString() {
      return "TramoOperacion [limiteInferior=" + limiteInferior + ", limiteSuperior=" + limiteSuperior + ", costeLineal=" + costeLineal
          + ", costePorcentual=" + costePorcentual + "]";
    }

  }

}
