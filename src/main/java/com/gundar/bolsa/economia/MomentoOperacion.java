package com.gundar.bolsa.economia;

import java.util.List;

import com.gundar.bolsa.Indices.IndicesDia;
import com.gundar.bolsa.economia.ahorros.CambioAhorros;
import com.gundar.bolsa.simulacion.Estado;

public interface MomentoOperacion {

  public List<CambioAhorros> calculaCambioAhorros(Estado estadoActual, IndicesDia indicesDia, boolean permiteInverso);

  public double calculaCambioDiario(Estado estadoActual, IndicesDia indicesDia, boolean permiteInverso);
}
