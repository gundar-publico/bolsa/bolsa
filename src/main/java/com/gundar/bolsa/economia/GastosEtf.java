package com.gundar.bolsa.economia;

import java.util.Collections;
import java.util.List;

import com.gundar.bolsa.economia.ahorros.Ahorros;
import com.gundar.bolsa.economia.ahorros.CambioAhorros;
import com.gundar.bolsa.economia.tarifas.TarifasBroker;

public class GastosEtf extends Gastos {

  private final TarifasBroker tarifasBroker;

  public GastosEtf(TarifasBroker tarifas) {
    this(tarifas, null);
  }

  public GastosEtf(TarifasBroker tarifas, Gastos gastosEncadenados) {
    super(gastosEncadenados);
    this.tarifasBroker = tarifas;
  }

  @Override
  protected void gastosPropiosEntrada(Ahorros ahorros) {
    gastosPropios(ahorros);
  }

  @Override
  protected void gastosPropiosSalida(Ahorros ahorros) {
    gastosPropios(ahorros);
  }

  private void gastosPropios(Ahorros ahorros) {

    double gastos = tarifasBroker.calcularGastos(ahorros.getCantidad());
    double nuevaCantidad = ahorros.getCantidad() - gastos;

    ahorros.setCantidad(nuevaCantidad);
  }

  @Override
  protected List<CambioAhorros> gastosPropiosEntrada() {
    return gastosPropios();
  }

  @Override
  protected List<CambioAhorros> gastosPropiosSalida() {
    return gastosPropios();
  }

  private List<CambioAhorros> gastosPropios() {
    return Collections.singletonList(tarifasBroker);
  }

}
