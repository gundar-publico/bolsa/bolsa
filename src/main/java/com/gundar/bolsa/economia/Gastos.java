package com.gundar.bolsa.economia;

import java.util.ArrayList;
import java.util.List;

import com.gundar.bolsa.economia.ahorros.Ahorros;
import com.gundar.bolsa.economia.ahorros.CambioAhorros;

public abstract class Gastos {

  private final Gastos gastosEncadenados;

  public Gastos(Gastos gastosEncadenados) {
    this.gastosEncadenados = gastosEncadenados;
  }

  public void entrada(Ahorros ahorros) {

    gastosPropiosEntrada(ahorros);

    if (gastosEncadenados != null) {
      gastosEncadenados.entrada(ahorros);
    }
  }

  public List<CambioAhorros> entrada() {

    List<CambioAhorros> gastos = gastosPropiosEntrada();

    List<CambioAhorros> gastosTotales = new ArrayList<>(gastos);

    if (gastosEncadenados != null) {
      gastosTotales.addAll(gastosEncadenados.entrada());
    }

    return gastosTotales;
  }

  public void salida(Ahorros ahorros) {

    gastosPropiosSalida(ahorros);

    if (gastosEncadenados != null) {
      gastosEncadenados.salida(ahorros);
    }
  }

  public List<CambioAhorros> salida() {

    List<CambioAhorros> gastos = gastosPropiosSalida();

    List<CambioAhorros> gastosTotales = new ArrayList<>(gastos);

    if (gastosEncadenados != null) {
      gastosTotales.addAll(gastosEncadenados.salida());
    }

    return gastosTotales;
  }

  protected abstract void gastosPropiosEntrada(Ahorros ahorros);

  protected abstract List<CambioAhorros> gastosPropiosEntrada();

  protected abstract void gastosPropiosSalida(Ahorros ahorros);

  protected abstract List<CambioAhorros> gastosPropiosSalida();
}
