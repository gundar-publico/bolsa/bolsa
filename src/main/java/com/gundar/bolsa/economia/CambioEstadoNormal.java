package com.gundar.bolsa.economia;

import com.gundar.bolsa.simulacion.Accion;
import com.gundar.bolsa.simulacion.Estado;

public class CambioEstadoNormal implements CambioEstado {

  @Override
  public Estado actualizar(Estado estadoSinActualizar, Accion accion) {
    return estadoSinActualizar.actualizar(accion);
  }
}
