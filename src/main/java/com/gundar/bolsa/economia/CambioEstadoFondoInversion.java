package com.gundar.bolsa.economia;

import com.gundar.bolsa.simulacion.Accion;
import com.gundar.bolsa.simulacion.Estado;

public class CambioEstadoFondoInversion implements CambioEstado {

  private OperativaFondo fondoRV;
  private OperativaFondo fondoRefugio;

  private int diasParaEntrarSalir;
  private int diasEstadoBloqueado;

  private Estado estadoTransicion;
  private Accion accionPendiente;

  public CambioEstadoFondoInversion(OperativaFondo fondoRv, OperativaFondo fondoRefugio) {
    this.fondoRV = fondoRv;
    this.fondoRefugio = fondoRefugio;
    reset();
  }

  @Override
  public Estado actualizar(Estado estadoSinActualizar, Accion accion) {

    Estado estadoActualizado = estadoSinActualizar.actualizar(accion);

    if (enTransicion()) {

      estadoActualizado = transicion(estadoSinActualizar, accion);

    }

    if (!enTransicion()) {

      if (estadoActualizado == Estado.ENTRANDO) {

        // entrar();
        movimiento(fondoRefugio, fondoRV, estadoActualizado);
        estadoActualizado = estadoSinActualizar;

      } else if (estadoActualizado == Estado.SALIENDO) {

        // salir();
        movimiento(fondoRV, fondoRefugio, estadoActualizado);
        estadoActualizado = estadoSinActualizar;

      } else {

        assert (estadoActualizado == Estado.DENTRO) || (estadoActualizado == Estado.FUERA);
        estadoActualizado = estadoSinActualizar.actualizar(accion);
      }

    }

    return estadoActualizado;
  }

  private void movimiento(OperativaFondo fondoOrigen, OperativaFondo fondoDestino, Estado estado) {

    diasParaEntrarSalir = fondoOrigen.getFechaContratacionSubscripcion();
    diasEstadoBloqueado = fondoOrigen.getDiasTotalesOperativa() + fondoDestino.getDiasTotalesOperativa();
    estadoTransicion = estado;
  }

  private Estado transicion(Estado estadoSinActualizar, Accion accion) {

    Estado estadoActualizado = estadoSinActualizar.actualizar(accion);

    if (diasEstadoBloqueado > 0) {

      if (accion != Accion.MANTENER) {
        accionPendiente = accion;
      }

      if (diasParaEntrarSalir <= 0) {
        estadoActualizado = estadoTransicion;
        estadoTransicion = estadoTransicion.actualizar(Accion.MANTENER);
      } else {
        /* Si aún no se han superado los días para la entrada o salida, mantenemos el estado anterior */
        estadoActualizado = estadoSinActualizar;
      }

      diasEstadoBloqueado--;
      diasParaEntrarSalir--;

    } else if (diasEstadoBloqueado == 0) {

      if (accion != Accion.MANTENER) {
        accionPendiente = accion;
      }

      if (accionPendiente != null) {
        estadoActualizado = estadoTransicion.actualizar(accionPendiente);
      }

      reset();
    }

    return estadoActualizado;
  }

  private boolean enTransicion() {

    boolean enTransicion = estadoTransicion != null;

    if (enTransicion) {
      assert diasEstadoBloqueado != -1;
    }

    return enTransicion;
  }

  private void reset() {
    diasParaEntrarSalir = -1;
    diasEstadoBloqueado = -1;

    accionPendiente = null;
    estadoTransicion = null;
  }

  public static class OperativaFondo {

    private int fechaContratacionSubscripcion;
    private int diasLiquidativosSubscripcion;

    public OperativaFondo(int fechaContratacionSubscripcion, int diasLiquidativosSubscripcion) {
      super();
      this.fechaContratacionSubscripcion = fechaContratacionSubscripcion;
      this.diasLiquidativosSubscripcion = diasLiquidativosSubscripcion;
    }

    public int getDiasTotalesOperativa() {
      return fechaContratacionSubscripcion + diasLiquidativosSubscripcion;
    }

    public int getFechaContratacionSubscripcion() {
      return fechaContratacionSubscripcion;
    }

    public int getDiasLiquidativosSubscripcion() {
      return diasLiquidativosSubscripcion;
    }

  }
}
