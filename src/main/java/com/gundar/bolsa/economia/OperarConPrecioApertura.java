package com.gundar.bolsa.economia;

import java.util.LinkedList;
import java.util.List;

import com.gundar.bolsa.Indices.IndicesDia;
import com.gundar.bolsa.economia.ahorros.CambioAhorros;
import com.gundar.bolsa.economia.ahorros.CambioAhorrosPorcentual;
import com.gundar.bolsa.InformacionDia;
import com.gundar.bolsa.simulacion.Estado;


public class OperarConPrecioApertura implements MomentoOperacion {

  private Gastos gastosDirecto;
  private Gastos gastosInverso;

  public OperarConPrecioApertura(Gastos gastos) {
    this(gastos, gastos);
  }

  public OperarConPrecioApertura(Gastos gastosDirecto, Gastos gastosInverso) {
    super();
    this.gastosDirecto = gastosDirecto;
    this.gastosInverso = gastosInverso;
  }

  @Override
  public List<CambioAhorros> calculaCambioAhorros(Estado estadoActual, IndicesDia indicesDia, boolean permiteInverso) {

    List<CambioAhorros> cambiosAhorros = new LinkedList<>();

    if (estadoActual == Estado.DENTRO) {

      final double cambioBolsaPorcentual;

      if (indicesDia.getIndiceDirecto() == null) {
        cambioBolsaPorcentual = 2 * indicesDia.getIndicePrincipal().getDiferenciaPorcentual();
      } else {
        cambioBolsaPorcentual = indicesDia.getIndiceDirecto().getDiferenciaPorcentual();
      }

      CambioAhorros cambioDiarioBolsa = new CambioAhorrosPorcentual(cambioBolsaPorcentual);
      cambiosAhorros.add(cambioDiarioBolsa);

    } else if (estadoActual == Estado.ENTRANDO) {

      if (permiteInverso) {

        final double cambioBolsaInversoPorcentual;

        if (indicesDia.getIndiceInverso() == null) {
          cambioBolsaInversoPorcentual = -2 * calculaDiferenciaPorcentualApertura(indicesDia.getIndicePrincipal());
        } else {
          cambioBolsaInversoPorcentual = calculaDiferenciaPorcentualApertura(indicesDia.getIndiceInverso());
        }

        CambioAhorros cambioDiarioInversoBolsa = new CambioAhorrosPorcentual(cambioBolsaInversoPorcentual);
        cambiosAhorros.add(cambioDiarioInversoBolsa);

        cambiosAhorros.addAll(gastosInverso.salida());
      }

      cambiosAhorros.addAll(gastosDirecto.entrada());

      final double cambioBolsaDirectoPorcentual;

      if (indicesDia.getIndiceDirecto() == null) {
        cambioBolsaDirectoPorcentual = 2 * calculaDiferenciaPorcentualDesdeApertura(indicesDia.getIndicePrincipal());
      } else {
        cambioBolsaDirectoPorcentual = calculaDiferenciaPorcentualDesdeApertura(indicesDia.getIndiceDirecto());
      }

      CambioAhorros cambioDiarioDirectoBolsa = new CambioAhorrosPorcentual(cambioBolsaDirectoPorcentual);
      cambiosAhorros.add(cambioDiarioDirectoBolsa);

    } else if (estadoActual == Estado.FUERA) {

      if (permiteInverso) {

        final double cambioBolsaPorcentual;

        if (indicesDia.getIndiceInverso() == null) {
          cambioBolsaPorcentual = -2 * indicesDia.getIndicePrincipal().getDiferenciaPorcentual();
        } else {
          cambioBolsaPorcentual = indicesDia.getIndiceInverso().getDiferenciaPorcentual();
        }

        CambioAhorros cambioDiarioBolsa = new CambioAhorrosPorcentual(cambioBolsaPorcentual);
        cambiosAhorros.add(cambioDiarioBolsa);
      }

    } else {
      assert estadoActual == Estado.SALIENDO;

      final double cambioBolsaDirectoPorcentual;

      if (indicesDia.getIndiceDirecto() == null) {
        cambioBolsaDirectoPorcentual = 2 * calculaDiferenciaPorcentualApertura(indicesDia.getIndicePrincipal());
      } else {
        cambioBolsaDirectoPorcentual = calculaDiferenciaPorcentualApertura(indicesDia.getIndiceDirecto());
      }

      CambioAhorros cambioDiarioDirectoBolsa = new CambioAhorrosPorcentual(cambioBolsaDirectoPorcentual);
      cambiosAhorros.add(cambioDiarioDirectoBolsa);

      cambiosAhorros.addAll(gastosDirecto.salida());

      if (permiteInverso) {

        cambiosAhorros.addAll(gastosInverso.entrada());

        final double cambioBolsaInversoPorcentual;

        if (indicesDia.getIndiceInverso() == null) {
          cambioBolsaInversoPorcentual = -2 * calculaDiferenciaPorcentualDesdeApertura(indicesDia.getIndicePrincipal());
        } else {
          cambioBolsaInversoPorcentual = calculaDiferenciaPorcentualDesdeApertura(indicesDia.getIndiceInverso());
        }

        CambioAhorros cambioDiarioInversoBolsa = new CambioAhorrosPorcentual(cambioBolsaInversoPorcentual);
        cambiosAhorros.add(cambioDiarioInversoBolsa);
      }
    }

    return cambiosAhorros;
  }

  @Override
  public double calculaCambioDiario(Estado estadoActual, IndicesDia indicesDia, boolean permiteInverso) {

    final double cambioBolsaPorcentual;

    if (estadoActual.enMercado()) {

      if (indicesDia.getIndiceDirecto() == null) {
        cambioBolsaPorcentual = 2 * indicesDia.getIndicePrincipal().getDiferenciaPorcentual();
      } else {
        cambioBolsaPorcentual = indicesDia.getIndiceDirecto().getDiferenciaPorcentual();
      }

    } else {

      if (permiteInverso) {

        if (indicesDia.getIndiceInverso() == null) {
          cambioBolsaPorcentual = -2 * indicesDia.getIndicePrincipal().getDiferenciaPorcentual();
        } else {
          cambioBolsaPorcentual = indicesDia.getIndiceInverso().getDiferenciaPorcentual();
        }

      } else {
        cambioBolsaPorcentual = 0;
      }

    }

    return cambioBolsaPorcentual;
  }

  private double calculaDiferenciaPorcentualDesdeApertura(InformacionDia informacionDia) {

    final double diferenciaPorcentualDesdeApertura;

    if ((informacionDia.getPrecioApertura() == null) || (Double.compare(informacionDia.getPrecioApertura(), 0) == 0)) {
      /* Si no hay precio de apertura, se coge la variación desde el precioCierre de cierre de ayer */
      diferenciaPorcentualDesdeApertura = informacionDia.getDiferenciaPorcentual();
    } else {

      double diferenciaLinealDesdeApertua = informacionDia.getPrecioCierre() - informacionDia.getPrecioApertura();
      diferenciaPorcentualDesdeApertura = diferenciaLinealDesdeApertua / informacionDia.getPrecioApertura();
    }

    return diferenciaPorcentualDesdeApertura;
  }

  private double calculaDiferenciaPorcentualApertura(InformacionDia informacionDia) {

    final double diferenciaPorcentualApertura;

    if ((informacionDia.getPrecioApertura() == null) || (Double.compare(informacionDia.getPrecioApertura(), 0) == 0)) {
      /* Si no hay precio de apertura, se entiende que el precio es el mismo que el de cierre anterior, y por tanto no hay variación */
      diferenciaPorcentualApertura = 0;
    } else {

      double precioCierreAnterior = informacionDia.getPrecioCierre() - informacionDia.getDiferenciaLineal();

      double diferenciaLinealApertura = informacionDia.getPrecioApertura() - precioCierreAnterior;
      diferenciaPorcentualApertura = diferenciaLinealApertura / precioCierreAnterior;
    }

    return diferenciaPorcentualApertura;
  }

}
