package com.gundar.bolsa.economia;

import com.gundar.bolsa.Indices.IndicesDia;
import com.gundar.bolsa.InformacionDia;
import com.gundar.bolsa.economia.CambioEstadoFondoInversion.OperativaFondo;
import com.gundar.bolsa.economia.ahorros.Ahorros;
import com.gundar.bolsa.economia.impuestos.ImpuestosFijos;
import com.gundar.bolsa.economia.tarifas.TarifasDeGiro;
import com.gundar.bolsa.simulacion.Accion;
import com.gundar.bolsa.simulacion.Estado;
import com.gundar.bolsa.simulacion.configuracion.ConfiguracionAhorros;

public class EstrategiaEconomica1 implements EstrategiaEconomica {

  private Ahorros ahorros;

  private Gastos gastosEntrada;
  private Gastos gastosSalida;

  private CambioEstado cambioEstado;
  private final MomentoOperacion momentoOperacion;

  private final boolean permiteInverso;

  public EstrategiaEconomica1(ConfiguracionAhorros configuracionAhorros, Gastos gastosEntrada, Gastos gastosSalida,
      CambioEstado cambioEstado, MomentoOperacion momentoOperacion, boolean permiteInverso) {

    this.ahorros = new Ahorros(configuracionAhorros);
    this.gastosEntrada = gastosEntrada;
    this.gastosSalida = gastosSalida;
    this.cambioEstado = cambioEstado;
    this.momentoOperacion = momentoOperacion;
    this.permiteInverso = permiteInverso;
  }

  public static EstrategiaEconomica etf(ConfiguracionAhorros configuracionAhorros, Gastos gastos, MomentoOperacion momentoOperacion,
      boolean permiteInverso) {

    return new EstrategiaEconomica1(
        configuracionAhorros,
        gastos,
        gastos,
        new CambioEstadoNormal(),
        momentoOperacion,
        permiteInverso);
  }

  public static EstrategiaEconomica etf(ConfiguracionAhorros configuracionAhorros, boolean permiteInverso) {

    return new EstrategiaEconomica1(
        configuracionAhorros,
        new GastosEtf(new TarifasDeGiro(), new GastosImpuestosPlusvalias(new ImpuestosFijos(0.21))),
        new GastosEtf(new TarifasDeGiro(), new GastosImpuestosPlusvalias(new ImpuestosFijos(0.21))),
        new CambioEstadoNormal(),
        new OperarConPrecioCierre(new GastosFondoInversion(), new GastosFondoInversion()),
        permiteInverso);
  }

  public static EstrategiaEconomica fondoInversion(ConfiguracionAhorros configuracionAhorros, boolean permiteInverso) {

    return new EstrategiaEconomica1(
        configuracionAhorros,
        new GastosFondoInversion(),
        new GastosFondoInversion(),
        new CambioEstadoFondoInversion(new OperativaFondo(0, 1), new OperativaFondo(0, 1)),
        new OperarConPrecioCierre(new GastosFondoInversion(), new GastosFondoInversion()),
        permiteInverso);
  }

  @Override
  public Estado cambio(IndicesDia indicesDia, Estado estadoSinActualizar, Accion accion) {

    InformacionDia informacionDia = indicesDia.getIndicePrincipal();

    ahorros.ingresoPeriodico(informacionDia.getFecha());

    Estado estadoActualizado = cambioEstado.actualizar(estadoSinActualizar, accion);

    if (estadoActualizado == Estado.ENTRANDO) {

      if (permiteInverso) {
        gastosSalida.salida(ahorros);
      }

      gastosEntrada.entrada(ahorros);

    } else if (estadoActualizado == Estado.SALIENDO) {

      gastosSalida.salida(ahorros);

      if (permiteInverso) {
        gastosEntrada.entrada(ahorros);
      }
    }

    double cambioDiarioPorcentual = momentoOperacion.calculaCambioDiario(estadoActualizado, indicesDia, permiteInverso);
    ahorros.cambioDiarioPorcentual(cambioDiarioPorcentual);

    return estadoActualizado;
  }

  @Override
  public Ahorros getAhorros() {
    return ahorros;
  }

}
