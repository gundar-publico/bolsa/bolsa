package com.gundar.bolsa.economia.impuestos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.gundar.bolsa.economia.ahorros.Ahorros;

public class ImpuestosPorTramos implements Impuestos {

  private final List<TramoImpuesto> tramosImpuestosOrdenados;

  public ImpuestosPorTramos(Collection<TramoImpuesto> tramosImpuestos) {
    super();
    this.tramosImpuestosOrdenados = new ArrayList<>(tramosImpuestos);
  }

  @Override
  public void cambio(Ahorros ahorros) {

    double gastos = calcular(ahorros.getCantidad());
    double nuevaCantidad = ahorros.getCantidad() - gastos;

    ahorros.setCantidad(nuevaCantidad);
  }

  @Override
  public double calcular(double plusvalia) {

    double impuestosPlusvalias = 0;

    if (plusvalia > 0) {

      for (TramoImpuesto tramo : tramosImpuestosOrdenados) {
        impuestosPlusvalias += tramo.calcula(plusvalia);
      }
    }

    return impuestosPlusvalias;
  }

  @Override
  public String toString() {
    return "ImpuestosPorTramos [tramosImpuestosOrdenados=" + tramosImpuestosOrdenados + "]";
  }

  static class TramoImpuesto implements Comparable<TramoImpuesto> {

    private int limiteInferior; // < plusvalia
    private int limiteSuperior; // >= plusvalia

    private double porcentaje;

    public TramoImpuesto(int limiteInferior, int limiteSuperior, double porcentaje) {
      super();
      this.limiteInferior = limiteInferior;
      this.limiteSuperior = limiteSuperior;
      this.porcentaje = porcentaje;
    }

    public double calcula(double plusvalia) {

      double plusvaliaTramo = calcularPlusvaliaTramo(plusvalia);

      return plusvaliaTramo * porcentaje;
    }

    private double calcularPlusvaliaTramo(double plusvalia) {

      /* Si no se llega al nivel mínimo de este tramo, se ignora */
      if (plusvalia <= limiteInferior) {
        return 0;
      }

      final double valorSuperiorTramo;

      if (plusvalia <= limiteSuperior) {
        valorSuperiorTramo = plusvalia;
      } else {
        /* Si se supera este tramo sólo se tiene en cuenta hasta su límite el máximo */
        valorSuperiorTramo = limiteSuperior;
      }

      double plusvaliaTramo = valorSuperiorTramo - limiteInferior;

      return plusvaliaTramo;
    }

    @Override
    public int compareTo(TramoImpuesto otroTramo) {

      return limiteInferior - otroTramo.limiteInferior;
    }

    @Override
    public String toString() {
      return "TramoImpuesto [limiteInferior=" + limiteInferior + ", limiteSuperior=" + limiteSuperior + ", porcentaje=" + porcentaje + "]";
    }

  }

}
