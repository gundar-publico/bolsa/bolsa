package com.gundar.bolsa.economia.impuestos;

import java.util.Arrays;
import java.util.Collection;

public class ImpuestosAcciones2018 extends ImpuestosPorTramos {

  public ImpuestosAcciones2018() {
    super(crearTramos());
  }

  private static Collection<TramoImpuesto> crearTramos() {

    Collection<TramoImpuesto> tramosImpuestos = Arrays.asList(
        new TramoImpuesto(0, 6_000, 0.19),
        new TramoImpuesto(6_000, 50_000, 0.21),
        new TramoImpuesto(50_000, Integer.MAX_VALUE, 0.23));

    return tramosImpuestos;
  }

}
