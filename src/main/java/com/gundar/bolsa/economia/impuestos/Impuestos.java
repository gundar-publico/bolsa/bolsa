package com.gundar.bolsa.economia.impuestos;

import com.gundar.bolsa.economia.ahorros.CambioAhorros;

public interface Impuestos extends CambioAhorros {

  public double calcular(double plusvalia);

}
