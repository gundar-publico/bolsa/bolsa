package com.gundar.bolsa.economia.impuestos;

import com.gundar.bolsa.economia.ahorros.Ahorros;
import com.gundar.bolsa.economia.ahorros.AhorrosCicloInversion;

public class ImpuestosFijos implements Impuestos {

  private final double porcentaje;

  public ImpuestosFijos(double porcentaje) {
    this.porcentaje = porcentaje;
  }

  @Override
  public void cambio(Ahorros ahorros) {

    double plusvalias = calculoPlusvalias(ahorros);
    double impuestosPlusvalias = calcular(plusvalias);

    double nuevaCantidad = ahorros.getCantidad() - impuestosPlusvalias;
    ahorros.setCantidad(nuevaCantidad);
  }

  @Override
  public double calcular(double plusvalia) {

    double impuestosPlusvalias = 0;

    if (plusvalia > 0) {

      impuestosPlusvalias = plusvalia * porcentaje;
    }

    return impuestosPlusvalias;
  }

  private double calculoPlusvalias(Ahorros ahorros) {

    AhorrosCicloInversion ahorrosCiclo = ahorros.getAhorrosCiclo();

    double cantidadInvertida = ahorrosCiclo.getCantidadInvertida();
    double cantidadFinal = ahorros.getCantidad();

    double plusvalias = cantidadFinal - cantidadInvertida;
    return plusvalias;
  }

  @Override
  public String toString() {
    return "ImpuestosFijos [porcentaje=" + porcentaje + "]";
  }

}
