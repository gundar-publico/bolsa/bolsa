package com.gundar.bolsa.economia;

import com.gundar.bolsa.Indices.IndicesDia;
import com.gundar.bolsa.economia.ahorros.Ahorros;
import com.gundar.bolsa.simulacion.Accion;
import com.gundar.bolsa.simulacion.Estado;

public interface EstrategiaEconomica {

  public Estado cambio(IndicesDia indicesDia, Estado estadoSinActualizar, Accion accion);

  public Ahorros getAhorros();

}
