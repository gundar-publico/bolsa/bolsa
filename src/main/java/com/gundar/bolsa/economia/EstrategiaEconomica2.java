package com.gundar.bolsa.economia;

import java.util.List;

import com.gundar.bolsa.Indices.IndicesDia;
import com.gundar.bolsa.economia.ahorros.Ahorros;
import com.gundar.bolsa.economia.ahorros.CambioAhorros;
import com.gundar.bolsa.InformacionDia;
import com.gundar.bolsa.simulacion.Accion;
import com.gundar.bolsa.simulacion.Estado;

public class EstrategiaEconomica2 implements EstrategiaEconomica {

  private Ahorros ahorros;

  private CambioEstado cambioEstado;
  private final MomentoOperacion momentoOperacion;

  private final boolean permiteInverso;

  public EstrategiaEconomica2(Ahorros ahorros,
      CambioEstado cambioEstado, MomentoOperacion momentoOperacion, boolean permiteInverso) {

    this.ahorros = ahorros;
    this.cambioEstado = cambioEstado;
    this.momentoOperacion = momentoOperacion;
    this.permiteInverso = permiteInverso;
  }

  @Override
  public Estado cambio(IndicesDia indicesDia, Estado estadoSinActualizar, Accion accion) {

    InformacionDia informacionDia = indicesDia.getIndicePrincipal();

    ahorros.ingresoPeriodico(informacionDia.getFecha());

    Estado estadoActualizado = cambioEstado.actualizar(estadoSinActualizar, accion);

    List<CambioAhorros> cambiosAhorros = momentoOperacion.calculaCambioAhorros(estadoActualizado, indicesDia, permiteInverso);

    for (CambioAhorros cambioAhorros : cambiosAhorros) {
      cambioAhorros.cambio(ahorros);
    }

    return estadoActualizado;
  }

  @Override
  public Ahorros getAhorros() {
    return ahorros;
  }

}
