package com.gundar.bolsa.economia;

import java.util.Collections;
import java.util.List;

import com.gundar.bolsa.economia.ahorros.Ahorros;
import com.gundar.bolsa.economia.ahorros.AhorrosCicloInversion;
import com.gundar.bolsa.economia.ahorros.CambioAhorros;
import com.gundar.bolsa.economia.impuestos.Impuestos;

public class GastosImpuestosPlusvalias extends Gastos {

  private final Impuestos impuestos;
  // private double plusvaliasCompensables;

  public GastosImpuestosPlusvalias(Impuestos impuestos) {
    this(null, impuestos);
  }

  public GastosImpuestosPlusvalias(Gastos gastosEncadenados, Impuestos impuestos) {
    super(gastosEncadenados);
    this.impuestos = impuestos;
  }

  @Override
  public void gastosPropiosEntrada(Ahorros ahorros) {
    // ahorros.getAhorrosCiclo().reset(ahorros.getCantidad());
    ahorros.resetCicloInversion();
  }

  @Override
  public void gastosPropiosSalida(Ahorros ahorros) {

    double plusvalias = calculoPlusvalias(ahorros);
    // double plusvaliasCompensadas = compensarPlusvalias(plusvalias);
    double plusvaliasCompensadas = plusvalias;
    double impuestosPlusvalias = impuestos.calcular(plusvaliasCompensadas);

    double nuevaCantidad = ahorros.getCantidad() - impuestosPlusvalias;
    ahorros.setCantidad(nuevaCantidad);
  }

  // private double compensarPlusvalias(double plusvalias) {
  //
  // final double plusvaliasCompensadas;
  //
  // if (plusvalias <= 0) {
  // plusvaliasCompensables += -plusvalias;// Como son pérdidas hay que pasarlas a positivo
  // plusvaliasCompensadas = 0;
  //
  // } else {
  //
  // if (plusvalias >= plusvaliasCompensables) {
  //
  // plusvaliasCompensadas = plusvalias - plusvaliasCompensables;
  // plusvaliasCompensables = 0;
  //
  // } else {
  //
  // plusvaliasCompensadas = 0;// En este caso quedan compensadas todas las plusvalías
  // plusvaliasCompensables = plusvaliasCompensables - plusvalias;// Actualizamos las plusvalias compensables restantes
  // }
  // }
  //
  // return plusvaliasCompensadas;
  // }

  private double calculoPlusvalias(Ahorros ahorros) {

    AhorrosCicloInversion ahorrosCiclo = ahorros.getAhorrosCiclo();

    double cantidadInvertida = ahorrosCiclo.getCantidadInvertida();
    double cantidadFinal = ahorros.getCantidad();

    double plusvalias = cantidadFinal - cantidadInvertida;

    // if (plusvalias < 0) {
    // System.out.println("Perdidas = " + plusvalias);
    // plusvalias = 0;
    // }

    return plusvalias;
  }

  @Override
  protected List<CambioAhorros> gastosPropiosEntrada() {
    return Collections.emptyList();
  }

  @Override
  protected List<CambioAhorros> gastosPropiosSalida() {
    return Collections.emptyList();
  }

}
