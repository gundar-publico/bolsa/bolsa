package com.gundar.bolsa.economia.ahorros;

import java.time.LocalDate;
import java.time.Month;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gundar.bolsa.simulacion.configuracion.ConfiguracionAhorros;

public class Ahorros {

  public static final Logger log = LoggerFactory.getLogger(Ahorros.class);

  private double cantidadInvertida;
  private double cantidad;

  private final ConfiguracionAhorros configuracionAhorros;

  private Month mes;

  private AhorrosCicloInversion ahorrosCiclo;

  public Ahorros(ConfiguracionAhorros configuracionAhorros) {
    this.cantidadInvertida = configuracionAhorros.getIngresoInicial();
    this.cantidad = configuracionAhorros.getIngresoInicial();
    this.configuracionAhorros = configuracionAhorros;
    this.ahorrosCiclo = new AhorrosCicloInversion(configuracionAhorros.getIngresoInicial());
  }

  public boolean ingresoPeriodico(LocalDate fecha) {

    boolean ingresoRealizado = false;

    if (cambioMes(fecha)) {

      cantidadInvertida += configuracionAhorros.getIngresoMensual();
      cantidad += configuracionAhorros.getIngresoMensual();
      ingresoRealizado = true;

      ahorrosCiclo.addInversion(configuracionAhorros.getIngresoMensual());

      if (log.isDebugEnabled()) {
        log.debug("Fecha = [" + fecha + "] ahorro mensual = [" + configuracionAhorros.getIngresoMensual() + "]. Total invertido = ["
            + cantidadInvertida + "]");
      }
    }

    return ingresoRealizado;
  }

  private boolean cambioMes(LocalDate fecha) {

    Month mesActual = fecha.getMonth();

    final boolean cambioMes;

    if (mes == null) {
      mes = mesActual;
    }

    if (mesActual == mes) {
      cambioMes = false;
    } else {
      cambioMes = true;
      mes = mesActual;
    }

    return cambioMes;
  }

  public void cambioDiarioPorcentual(double cambioPorcentual) {

    double cambioLineal = cantidad * cambioPorcentual;
    cantidad += cambioLineal;
  }

  public double calcularGananciaLineal() {
    return cantidad - cantidadInvertida;
  }

  public double calcularGananciaPorcentual() {

    double gananciaLineal = calcularGananciaLineal();
    double gananciaPorcentual = gananciaLineal / cantidadInvertida;

    return gananciaPorcentual;
  }

  public void resetCicloInversion() {
    ahorrosCiclo.reset(cantidad);
  }

  @Override
  public String toString() {
    return "Ahorros [cantidadInvertida=" + cantidadInvertida + ", cantidad=" + cantidad + "]";
  }

  public double getCantidadInvertida() {
    return cantidadInvertida;
  }

  public double getCantidad() {
    return cantidad;
  }

  public void setCantidad(double cantidad) {
    this.cantidad = cantidad;
  }

  public AhorrosCicloInversion getAhorrosCiclo() {
    return ahorrosCiclo;
  }

}
