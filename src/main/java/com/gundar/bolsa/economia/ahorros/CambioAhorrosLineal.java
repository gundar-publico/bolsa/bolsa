package com.gundar.bolsa.economia.ahorros;


public class CambioAhorrosLineal implements CambioAhorros {

  private final double cambio;

  public CambioAhorrosLineal(double cambio) {
    super();
    this.cambio = cambio;
  }

  @Override
  public void cambio(Ahorros ahorros) {

    double nuevaCantidad = ahorros.getCantidad() + cambio;

    ahorros.setCantidad(nuevaCantidad);
  }

  @Override
  public String toString() {
    return "CambioAhorrosLineal [cambio=" + cambio + "]";
  }

}
