package com.gundar.bolsa.economia.ahorros;


public class CambioAhorrosPorcentual implements CambioAhorros {

  private final double cambio;

  public CambioAhorrosPorcentual(double cambio) {
    super();
    this.cambio = cambio;
  }

  @Override
  public void cambio(Ahorros ahorros) {

    double cambioLineal = ahorros.getCantidad() * cambio;

    double nuevaCantidad = ahorros.getCantidad() + cambioLineal;

    ahorros.setCantidad(nuevaCantidad);
  }

  @Override
  public String toString() {
    return "CambioAhorrosPorcentual [cambio=" + cambio + "]";
  }

}
