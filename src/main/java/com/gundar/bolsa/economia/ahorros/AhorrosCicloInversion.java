package com.gundar.bolsa.economia.ahorros;

public class AhorrosCicloInversion {

  private double cantidadInvertida;

  public AhorrosCicloInversion(double cantidadInicial) {
    super();
    this.cantidadInvertida = cantidadInicial;
  }

  public void addInversion(double cantidad) {
    cantidadInvertida += cantidad;
  }

  public void reset(double nuevaCantidadInicial) {
    cantidadInvertida = nuevaCantidadInicial;
  }

  @Override
  public String toString() {
    return "AhorrosCicloInversion [cantidadInvertida=" + cantidadInvertida + "]";
  }

  public double getCantidadInvertida() {
    return cantidadInvertida;
  }
}
