package com.gundar.bolsa.economia.ahorros;


public class CambioAhorrosVacio implements CambioAhorros {

  public static final CambioAhorros INSTANCIA = new CambioAhorrosVacio();

  private CambioAhorrosVacio() {}

  @Override
  public void cambio(Ahorros ahorros) {}

}
