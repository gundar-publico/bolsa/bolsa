package com.gundar.bolsa.economia.ahorros;

public class ResetCicloInversion implements CambioAhorros {

  public static final ResetCicloInversion INSTANCIA = new ResetCicloInversion();

  @Override
  public void cambio(Ahorros ahorros) {
    // ahorros.getAhorrosCiclo().reset(ahorros.getCantidad());
    ahorros.resetCicloInversion();
  }

  @Override
  public String toString() {
    return "ResetCicloInversion []";
  }

}
