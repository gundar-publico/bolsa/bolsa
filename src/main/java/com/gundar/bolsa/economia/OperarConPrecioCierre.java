package com.gundar.bolsa.economia;

import java.util.LinkedList;
import java.util.List;

import com.gundar.bolsa.Indices.IndicesDia;
import com.gundar.bolsa.economia.ahorros.CambioAhorros;
import com.gundar.bolsa.economia.ahorros.CambioAhorrosPorcentual;
import com.gundar.bolsa.simulacion.Estado;


public class OperarConPrecioCierre implements MomentoOperacion {

  private Gastos gastosDirecto;
  private Gastos gastosInverso;

  public OperarConPrecioCierre(Gastos gastos) {
    this(gastos, gastos);
  }

  public OperarConPrecioCierre(Gastos gastosDirecto, Gastos gastosInverso) {
    super();
    this.gastosDirecto = gastosDirecto;
    this.gastosInverso = gastosInverso;
  }

  @Override
  public List<CambioAhorros> calculaCambioAhorros(Estado estadoActual, IndicesDia indicesDia, boolean permiteInverso) {

    List<CambioAhorros> cambiosAhorros = new LinkedList<>();

    if (estadoActual == Estado.ENTRANDO) {

      if (permiteInverso) {
        cambiosAhorros.addAll(gastosInverso.salida());
      }

      cambiosAhorros.addAll(gastosDirecto.entrada());

    } else if (estadoActual == Estado.SALIENDO) {

      cambiosAhorros.addAll(gastosDirecto.salida());

      if (permiteInverso) {
        cambiosAhorros.addAll(gastosInverso.entrada());
      }
    }

    double cambioBolsaPorcentual = calculaCambioDiario(estadoActual, indicesDia, permiteInverso);

    if (cambioBolsaPorcentual != 0) {

      CambioAhorros cambioDiarioBolsa = new CambioAhorrosPorcentual(cambioBolsaPorcentual);
      cambiosAhorros.add(cambioDiarioBolsa);
    }

    return cambiosAhorros;
  }

  @Override
  public double calculaCambioDiario(Estado estadoActual, IndicesDia indicesDia, boolean permiteInverso) {

    final double variacionDiaria;

    if (estadoActual.enMercado()) {

      if (indicesDia.getIndiceDirecto() == null) {
        variacionDiaria = 2 * indicesDia.getIndicePrincipal().getDiferenciaPorcentual();
      } else {
        variacionDiaria = indicesDia.getIndiceDirecto().getDiferenciaPorcentual();
      }

    } else if (permiteInverso) {

      if (indicesDia.getIndiceInverso() == null) {
        variacionDiaria = -2 * indicesDia.getIndicePrincipal().getDiferenciaPorcentual();
      } else {
        variacionDiaria = indicesDia.getIndiceInverso().getDiferenciaPorcentual();
      }

    } else {

      assert !estadoActual.enMercado() && !permiteInverso;
      variacionDiaria = 0;
    }

    return variacionDiaria;
  }

}
