package com.gundar.bolsa.economia;

import java.util.Collections;
import java.util.List;

import com.gundar.bolsa.economia.ahorros.Ahorros;
import com.gundar.bolsa.economia.ahorros.CambioAhorros;

public class GastosFondoInversion extends Gastos {

  public GastosFondoInversion() {
    this(null);
  }

  public GastosFondoInversion(Gastos gastosEncadenados) {
    super(gastosEncadenados);
  }

  @Override
  protected void gastosPropiosEntrada(Ahorros ahorros) {}

  @Override
  protected void gastosPropiosSalida(Ahorros ahorros) {}

  @Override
  protected List<CambioAhorros> gastosPropiosEntrada() {
    return Collections.emptyList();
  }

  @Override
  protected List<CambioAhorros> gastosPropiosSalida() {
    return Collections.emptyList();
  }

}
