package com.gundar.bolsa.economia;

import com.gundar.bolsa.simulacion.Accion;
import com.gundar.bolsa.simulacion.Estado;

public interface CambioEstado {

  public Estado actualizar(Estado estadoSinActualizar, Accion accion);

}
