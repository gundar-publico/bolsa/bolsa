package com.gundar.bolsa;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import com.gundar.bolsa.Indices.IndicesDia;

public class Indices implements Iterable<IndicesDia> {

  private List<InformacionDia> indicePrincipal;

  private Map<LocalDate, InformacionDia> indiceDirecto;

  private Map<LocalDate, InformacionDia> indiceInverso;

  public Indices(List<InformacionDia> indicePrincipal, List<InformacionDia> indiceDirecto, List<InformacionDia> indiceInverso) {

    this.indicePrincipal = indicePrincipal;
    this.indiceDirecto = listToMap(indiceDirecto);
    this.indiceInverso = listToMap(indiceInverso);
  }

  private Map<LocalDate, InformacionDia> listToMap(List<InformacionDia> lista) {

    if (lista == null) {
      return Collections.emptyMap();
    }

    Map<LocalDate, InformacionDia> mapa = new HashMap<>(lista.size());

    for (InformacionDia infoDia : lista) {

      if (mapa.containsKey(infoDia.getFecha())) {
        throw new IllegalStateException("Fecha ya contenida = [" + infoDia.getFecha() + "]");
      }

      mapa.put(infoDia.getFecha(), infoDia);
    }

    return mapa;
  }

  public List<InformacionDia> getIndicePrincipal() {
    return indicePrincipal;
  }

  @Override
  public Iterator<IndicesDia> iterator() {
    return new IndicesIterator();
  }

  public static class IndicesDia {

    private InformacionDia indicePrincipal;
    private InformacionDia indiceDirecto;
    private InformacionDia indiceInverso;

    public IndicesDia(InformacionDia indicePrincipal, InformacionDia indiceDirecto, InformacionDia indiceInverso) {
      super();
      this.indicePrincipal = indicePrincipal;
      this.indiceDirecto = indiceDirecto;
      this.indiceInverso = indiceInverso;
    }

    @Override
    public String toString() {
      return "IndicesDia [indicePrincipal=" + indicePrincipal + ", indiceDirecto=" + indiceDirecto + ", indiceInverso=" + indiceInverso
          + "]";
    }

    public InformacionDia getIndicePrincipal() {
      return indicePrincipal;
    }

    public InformacionDia getIndiceDirecto() {
      return indiceDirecto;
    }

    public InformacionDia getIndiceInverso() {
      return indiceInverso;
    }

  }

  private class IndicesIterator implements Iterator<IndicesDia> {

    private Iterator<InformacionDia> iteradorPrincipal;

    public IndicesIterator() {
      this.iteradorPrincipal = indicePrincipal.iterator();
    }

    @Override
    public boolean hasNext() {
      return iteradorPrincipal.hasNext();
    }

    @Override
    public IndicesDia next() {

      if (!hasNext()) {
        throw new NoSuchElementException();
      }

      InformacionDia infoDiaPrincipal = iteradorPrincipal.next();
      InformacionDia infoDiaDirecto = indiceDirecto.get(infoDiaPrincipal.getFecha());
      InformacionDia infoDiaInverso = indiceInverso.get(infoDiaPrincipal.getFecha());

      return new IndicesDia(infoDiaPrincipal, infoDiaDirecto, infoDiaInverso);
    }

  }

}
